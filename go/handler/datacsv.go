package handler

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/labstack/echo"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Status struct {
	ID    string
	Pg    int8
	Fname string
}

type ReqInfo struct {
	Req   string
	ID    []string
	FNAME []string
	St    string
	Ed    string
	PG    []int
}

type Payloads struct {
	Ch   int
	Data []float64
}

type ResMSG struct {
	REQ   string
	FNAME []string
	PG    []int
}

type PostData struct {
	Id []string
	St string
	Ed string
}

type rData struct {
	Id       string
	Ch_count int
	Sampling int
	Payload  []Payloads
	UTC      int `json:"UTC" bson:"UTC"`
	Lat      float64
	Long     float64
	Batt     float64
	Time     string
}

type Pload struct {
	Id []string `json:"id" form:"id" query:"id"`
	St string   `json:"st" form:"st" query:"st"`
	Ed string   `json:"ed" form:"ed" query:"ed"`
}

const charset = "abcdefghijklmnopqrstuvwxyz" +
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

var seededRand *rand.Rand = rand.New(
	rand.NewSource(time.Now().UnixNano()))

func StringWithCharset(length int, charset string) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

func RandStringRunes(n int) string {
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func checkError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}

func saveReq(pp *Pload, h *Handler) (string, []string, []int) {
	db := h.DB.Clone()
	defer db.Close()

	stStr := strings.ReplaceAll(pp.St, ":", "-")
	stStr = strings.ReplaceAll(stStr, ".", "-")

	edStr := strings.ReplaceAll(pp.Ed, ":", "-")
	edStr = strings.ReplaceAll(edStr, ".", "-")

	a := []int{}
	b := []string{}
	// reqID := RandStringRunes(8)
	reqID := StringWithCharset(8, charset)

	for i := 0; i < len(pp.Id); i++ {
		a = append(a, 0)
		b = append(b, pp.Id[i]+"__"+stStr+"_"+edStr+".csv")
	}

	p := ReqInfo{
		Req:   reqID,
		FNAME: b,
		ID:    pp.Id,
		St:    pp.St,
		Ed:    pp.Ed,
		PG:    a,
	}

	if err := db.DB("ssmic").C("req").Insert(p); err != nil {

	}
	return reqID, b, a

}

func makeFile(id string, st string, ed string, results []*rData) {
	start := time.Now()
	//EX ST timeinput 2019-11-07T20:00:00.000
	stStr := strings.ReplaceAll(st, ":", "-")
	stStr = strings.ReplaceAll(stStr, ".", "-")

	edStr := strings.ReplaceAll(ed, ":", "-")
	edStr = strings.ReplaceAll(edStr, ".", "-")
	fname := results[0].Id + "__" + stStr + "_" + edStr + ".csv"
	fmt.Println(fname)

	file, err := os.Create(fname)
	checkError("Cannot create file", err)
	defer file.Close()

	file2, _ := os.OpenFile(fname, os.O_RDWR, 0644)

	defer file2.Close()
	for i := 0; i < len(results); i++ {
		// tmp := results[i].UTC
		tmpSamp := results[i].Sampling
		tmpUtc := results[i].UTC

		for j := 0; j < tmpSamp; j++ {
			timeMS := tmpUtc + j
			ts := time.Unix((int64)(timeMS/1000), (int64)((timeMS%1000)*1000000)).Format(time.RFC3339Nano)[0:23]
			s := fmt.Sprintf("%s,%f,%f,%f\n", ts, results[i].Payload[0].Data[j], results[i].Payload[1].Data[j], results[i].Payload[2].Data[j])
			_, err = file2.WriteString(s)
		}

		_ = file2.Sync()

	}
	fmt.Println("\r\n ID:" + id + " ,Save Complete!!!")
	// fmt.Println(time.Now().Format(time.RFC850))

	fmt.Println("SaveFile time: ", time.Since(start), " s")
}

func makeFile2(u *Pload, h *Handler, ReqID string, pG []int) {
	fmt.Println(pG)

	db := h.DB.Clone()
	defer db.Close()

	for i := 0; i < len(u.Id); i++ {
		fmt.Println(u.Id[i])
		var results []*rData
		var filter = bson.M{"id": u.Id[i], "Time": bson.M{"$gte": u.St, "$lte": u.Ed}}

		fmt.Println(filter)

		if err := db.DB("ssmic").C("data").
			Find(filter).All(&results); err != nil {
			if err == mgo.ErrNotFound {
				return
			}
			return
		}

		fmt.Printf("\r\nID "+u.Id[i]+",All data :%d\n", len(results))

		if len(results) > 0 {
			fmt.Printf("Id     : %s\n", results[0].Id)
			fmt.Printf("UTC    : %d\n", results[0].UTC)
			fmt.Printf("Time ST: %s\n", results[0].Time)
			fmt.Printf("Time ED: %s\n", results[len(results)-1].Time)
			fmt.Printf("Lat   : %f\n", results[len(results)-1].Lat)
			fmt.Printf("Long  : %f\n", results[len(results)-1].Long)
			// go makeFile(u.Id[i], u.St, u.Ed, results)

			start := time.Now()
			//EX ST timeinput 2019-11-07T20:00:00.000
			stStr := strings.ReplaceAll(u.St, ":", "-")
			stStr = strings.ReplaceAll(stStr, ".", "-")

			edStr := strings.ReplaceAll(u.Ed, ":", "-")
			edStr = strings.ReplaceAll(edStr, ".", "-")
			fname := results[0].Id + "__" + stStr + "_" + edStr + ".csv"
			// fpath := "./" + fname
			fpath := "/var/www/html/export/" + fname
			fmt.Println(fname)

			file, err := os.Create(fpath)
			checkError("Cannot create file", err)
			defer file.Close()

			file2, _ := os.OpenFile(fpath, os.O_RDWR, 0644)

			defer file2.Close()
			for i := 0; i < len(results); i++ {
				// tmp := results[i].UTC
				tmpSamp := results[i].Sampling
				tmpUtc := results[i].UTC

				for j := 0; j < tmpSamp; j++ {
					timeMS := tmpUtc + j
					ts := time.Unix((int64)(timeMS/1000), (int64)((timeMS%1000)*1000000)).Format(time.RFC3339Nano)[0:23]
					s := fmt.Sprintf("%s,%f,%f,%f\n", ts, results[i].Payload[0].Data[j], results[i].Payload[1].Data[j], results[i].Payload[2].Data[j])
					_, err = file2.WriteString(s)
				}

				_ = file2.Sync()

			}
			fmt.Println("\r\n ID:" + u.Id[i] + " ,Save Complete!!!")
			fmt.Println("SaveFile time: ", time.Since(start), " s")
			pG[i] = 100

			if err := db.DB("ssmic").C("req").Update(bson.M{"req": ReqID}, bson.M{"$set": bson.M{"pg": pG}}); err != nil {

			}

		} else {
			pG[i] = -1
			if err := db.DB("ssmic").C("req").Update(bson.M{"req": ReqID}, bson.M{"$set": bson.M{"pg": pG}}); err != nil {

			}

		}

	}
	fmt.Println("\r\n !!!! SaveAll Complete !!!!\r\n")
}

func (h *Handler) QueryData(c echo.Context) (err error) {

	u := new(Pload)
	if err := c.Bind(u); err != nil {
		return err
	}
	fmt.Println(u.Id)
	fmt.Println(u.St)
	fmt.Println(u.Ed)

	reqID, fileName, pG := saveReq(u, h)

	go makeFile2(u, h, reqID, pG)

	fmt.Println("Test GO routine")

	ret := ResMSG{REQ: reqID, FNAME: fileName, PG: pG}
	// ret := ResMSG{}
	return c.JSON(200, ret)
}

func (h *Handler) QueryStatus(c echo.Context) (err error) {
	reqId := c.Param("rid")
	u := new(ReqInfo)

	// Find user
	db := h.DB.Clone()
	defer db.Close()
	if err := db.DB("ssmic").C("req").
		Find(bson.M{"req": reqId}).One(u); err != nil {
		if err == mgo.ErrNotFound {
			return &echo.HTTPError{Code: http.StatusUnauthorized, Message: "Fail"}
		}

	}

	return c.JSON(http.StatusOK, u)
}
