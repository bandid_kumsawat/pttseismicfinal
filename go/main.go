package main

import (
	"fmt"
	"os"
	"strings"
	"time"

	"net/http"

	"./handler"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/labstack/gommon/log"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Payloads struct {
	Ch   int
	Data []float64
}

type ResMSG struct {
	Url string
}

type PostData struct {
	Id []string
	St string
	Ed string
}

type rData struct {
	Id       string
	Ch_count int
	Sampling int
	Payload  []Payloads
	UTC      int `json:"UTC" bson:"UTC"`
	Lat      float64
	Long     float64
	Batt     float64
	Time     string
}

type Pload struct {
	Id []string `json:"id" form:"id" query:"id"`
	St string   `json:"st" form:"st" query:"st"`
	Ed string   `json:"ed" form:"ed" query:"ed"`
}

func checkError(message string, err error) {
	if err != nil {
		log.Fatal(message, err)
	}
}

func exportFn(c echo.Context) error {
	mongo_host := "mongodb://admin:deaw1234@ssmonitors.site:27017"

	u := new(Pload)
	if err := c.Bind(u); err != nil {
		return err
	}
	fmt.Println(u.Id)

	session, err := mgo.Dial(mongo_host)
	defer session.Close()
	s := session.DB("ssmic").C("data")

	var results []*rData
	// var filter = bson.M{"id": "WS000001", "Time": bson.M{"$gte": "2019-11-07T19:45:00.000", "$lte": "2019-11-07T19:46:00.000"}}
	var filter = bson.M{"id": u.Id[0], "Time": bson.M{"$gte": u.St, "$lte": u.Ed}}

	// fmt.Println(filter)
	fmt.Println(time.Now().Format(time.RFC850))

	err = s.Find(filter).All(&results)
	if err != nil {

	}
	fmt.Printf("\r\nAll data :%d\n", len(results))

	fmt.Printf("Id     : %s\n", results[0].Id)
	fmt.Printf("UTC    : %d\n", results[0].UTC)
	fmt.Printf("Time ST: %s\n", results[0].Time)
	fmt.Printf("Time ED: %s\n", results[len(results)-1].Time)
	fmt.Printf("Lat   : %f\n", results[len(results)-1].Lat)
	fmt.Printf("Long  : %f\n", results[len(results)-1].Long)

	stStr := strings.ReplaceAll(u.St, ":", "-")
	stStr = strings.ReplaceAll(stStr, ".", "-")

	edStr := strings.ReplaceAll(u.Ed, ":", "-")
	edStr = strings.ReplaceAll(edStr, ".", "-")
	fname := results[0].Id + "__" + stStr + "_" + edStr + ".csv"
	fmt.Println(fname)

	file, err := os.Create(fname)
	checkError("Cannot create file", err)
	defer file.Close()

	file2, _ := os.OpenFile(fname, os.O_RDWR, 0644)

	defer file2.Close()
	for i := 0; i < len(results); i++ {
		// tmp := results[i].UTC
		tmp_samp := results[i].Sampling
		tmp_utc := results[i].UTC

		for j := 0; j < tmp_samp; j++ {
			time_ms := tmp_utc + j
			ts := time.Unix((int64)(time_ms/1000), (int64)((time_ms%1000)*1000000)).Format(time.RFC3339Nano)[0:23]
			s := fmt.Sprintf("%s,%f,%f,%f\n", ts, results[i].Payload[0].Data[j], results[i].Payload[1].Data[j], results[i].Payload[2].Data[j])
			_, err = file2.WriteString(s)
		}

		_ = file2.Sync()

	}
	fmt.Println("\r\nSave Complete!!!\r\n")
	fmt.Println(time.Now().Format(time.RFC850))

	ret := ResMSG{Url: "export/" + fname}
	return c.JSON(200, ret)
}

func main() {

	e := echo.New()
	e.Logger.SetLevel(log.ERROR)
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{http.MethodGet, http.MethodHead, http.MethodPut, http.MethodPatch, http.MethodPost, http.MethodDelete},
	}))

	// Database connection
	db, err := mgo.Dial("mongodb://admin:deaw1234@ssmonitors.site:27017")
	if err != nil {
		e.Logger.Fatal(err)
	}

	// Initialize handler
	h := &handler.Handler{DB: db}

	e.GET("/api/:rid", h.QueryStatus)
	e.POST("/api/export", h.QueryData)

	e.Logger.Fatal(e.Start(":3002"))
}

/*
func main() {

	e := echo.New()
	e.Logger.SetLevel(log.ERROR)
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})
	e.POST("/api/export", exportFn)

	e.Logger.Fatal(e.Start(":3002"))
}
*/
