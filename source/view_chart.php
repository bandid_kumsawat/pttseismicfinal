<?php
  session_start();
  if(isset($_SESSION["token"])) {
      $token = $_SESSION["token"];
  } else {
    header("Location: ../index.html");
    exit;
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <link rel="icon" href="../img/logo/logo-pttep.png" type="image/png" sizes="17x17">
  <title>WIRELESS LAND SEISMIC MONITORING</title>
  <!-- Font Awesome -->
  <link href="../font/fontawesome/css/all.min.css" rel="stylesheet">
  <!-- Bootstrap core CSS -->
  <link href="../css/bootstrap.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="../css/mdb.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="../css/style.min.css" rel="stylesheet">
  <!-- Your custom datetimepicker (optional) -->
  <link href="../css/jquery.datetimepicker.min.css" rel="stylesheet">
  <!-- Your custom dataTable (optional) -->
  <link href="../css/datatables.css" rel="stylesheet">
  <style>

  .map-container{
    overflow:hidden;
    padding-bottom:56.25%;
    position:relative;
    height:0;
  }
  .map-container iframe{
    left:0;
    top:0;
    height:100%;
    width:100%;
    position:absolute;
  }

  .tr_color {color: #F5F5F5;}

    /* new nav active */
    .nav-active{
    border-bottom:6px solid #000000;
    border-radius: 3px;
  }
  .nav-pills .nav-link.active,
  .nav-pills .show > .nav-link {
    border-bottom: 5px solid #007bff;
  }
  .nav-menu {
    color: #007bff;
  }

  .dropdown-menu {
    left: auto;
    right: 0;
    float: right;
  }

  .nav-pills .nav-link {
    border-radius: 0rem;
  }
  .bg-image {
    background-image: url(../login/images/BG_09.jpg);
  }
  .image-image-image{
    /* background-image: url(../login/images/522343-earthquake.jpg); */
  }
  </style>
</head>

<body class="grey lighten-3">

  <!--Main Navigation-->
  <header>

    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
      <div class="container-fluid">

        <!-- Collapse -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Links -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

          <!-- Left -->
          <ul class="nav nav-pills nav-pills-primary mr-auto" role="tablist">
            <li class="nav-item" style = "padding-right: 12px;">
              <a class="nav-link" style = "padding-bottom: 0px !important;padding-right: 4px !important;padding-left:4px !important;" href="./dashboard.php?timezone=<?php echo $_GET['timezone']; ?>" role="tablist" aria-expanded="true">
                <i class="fas fa-tram mr-1"></i> <B>DEVICE MANAGER</B>
              </a>
            </li>
            <!-- <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#link1" role="tablist" aria-expanded="false">
                <i class="fas fa-chart-bar mr-1"></i> <B>DASHBOARD</B>
              </a>
            </li> -->
            
            <li class="nav-item" style = "padding-right: 12px;">
              <a class="nav-link active" style = "padding-bottom: 0px !important;padding-right: 4px !important;padding-left:4px !important;" href='./view_chart.php?device=["WS000001"]&timezone=<?php echo $_GET['timezone']; ?>' role="tablist" aria-expanded="true">
                <i class="fas fa-chart-line mr-1"></i> <B>REAL TIME</B>
              </a>
            </li>

            <li class="nav-item" style = "padding-right: 12px;">
              <a class="nav-link " style = "padding-bottom: 0px !important;padding-right: 4px !important;padding-left:4px !important;" href="./rawdata.php?timezone=<?php echo $_GET['timezone']; ?>" role="tablist" aria-expanded="false">
                <i class="fas fa-table mr-1"></i><B>DAQ DATA</B>
              </a>
            </li>

          </ul>

          <!-- Right -->
          <ul class="nav nav-menu">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown">
                <i class="fas fa-address-card mr-1"></i><B><?php echo $_SESSION['email'] ?></B>
              </a>
              <div class="dropdown-menu">
                <center>
                  <B><?php echo $_SESSION['email'] ?></B>
                  <img src = "../login/images/admin.png" width="100px;"/>
                  <a class="dropdown-item">
                  <button id = "c_time"class='btn btn-outline-info btn-sm' data-toggle="modal" data-target="#change_datetime_show">Change Time</button><br>
                    <button id = "logout"class='btn btn-outline-danger btn-sm'>Logout</button>
                  </a>
              </center>
              </div>
            </li>
          </ul>

        </div>

      </div>
    </nav>
    <!-- Navbar -->

    <!-- Sidebar -->
    <div class="sidebar-fixed position-fixed bg-image">

      <!-- <a class="logo-wrapper waves-effect"> -->
      <div class="mb-4">
        <center><img src="../img/logo/logo-pttep.png"  alt="" style="width: 70%; height: 70%"></center>
      </div>
      <!-- </a> -->

      <!-- <M>Menu Config</M> -->
      <!-- <div class="mb-2"></div> -->

      <table class="table table-hover">
        <tbody>

          <tr>
            <th>Project</th>
            <td>WIRELESS LAND SEISMIC</td>
          </tr>
          <tr>
            <th>Date</th>
            <td id="real_date"></td>
          </tr>
          <!-- <tr>
            <th>Status</th>
            <td id="side_status"></td>
          </tr> -->
    </tbody>
  </table>
</div>
<!-- Sidebar -->

</header>
<!--Main Navigation-->

<!--Main layout-->
<main class="pt-5 mx-lg-5">
  <div class="container-fluid mt-5">
    <div class="tab-content tab-space">
    <!-- Page 2 -->
    <div class="tab-pane" id="link2" aria-expanded="true">
      <!-- Graph RAW -->
    
    </div>




    <div class="col-md-12  fadeIn">
      <div class="card ">
        <div class="container">
          <div class="col-12">
            <div class="row">
              <div class='col-12'>
                <center>
                  <p><div style = "color: black;font-size: 1.5em;font-weight: 700;">
                  <?php 
                    if ($_GET['timezone'] == "0"){
                      echo "Time Zone [ UTC ]";
                    }else if ($_GET['timezone'] == '7') {
                      echo "Time Zone [ GMT Thailand ]";
                    }
                  ?>
                </div></p>
              </center>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>




    <!-- CH 1 -->
    <div class="tab-pane active" id="link1" aria-expanded="true">
      <div class="col-md-12 mb-4 fadeIn" style = "padding:0px !important;">
      <div class="col-md-12 mb-4 fadeIn">
        <div class="row">
          <!-- <div class="col-md-12"></div> -->
          <div class="col-md-12  fadeIn">
            <div class="card ">
              <div class="container">
                <div class="col-12">
                  <div class="row">
                    <div class='col-3'></div>
                    <div class="col-4">
                    <p class="mt-2">SELECT DEVICE</p>
                      <div class="input-group mt-3 mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fas fa-microchip"></i><!-- <i class="fas fa-calendar-week"></i> --></span>
                        </div>
                        
                        <!-- <input type="text" class="form-control"  id="SELECT_DEVICE_ID" value=""> -->
                        <select id = 'SELECT_DEVICE_ID' class="browser-default custom-select">
                          <option value="0" selected>NOT FOUND DEVICE</option>
                          <!-- <option value="1">One</option>
                          <option value="2">Two</option>
                          <option value="3">Three</option> -->
                        </select>
                      </div>
                    </div>
                    <div class="col-2">
                      <div class="mt-4 mb-10" style = "margin-top:24px !important;">
                        <button type="button" class="btn blue-gradient btn-rounded waves-effect waves-light" id="view_device">VIEW</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>





        <div class="card">
        <div href="#CH1" class="card-header waves-effect" data-toggle="collapse" id="GCH1"><i class="fas fa-chart-line  fa-2x mr-2"></i><B id = "show_time_channel_1">CH 1</B></div>
          <div class="card-body collapse" id="CH1">
            <div id="ch1" style="/*height: 400px;*/ min-width: 310px">
                <input type="checkbox" name="index" value="" id="index" checked style="display:none;">
                <div class="col-md-12 mb-4 fadeIn">
                  <div class="container-fluid ">
                    <div class="card-body collapse" id="CH1">
                      <div id="ch1" style="height: 400px; min-width: 310px"></div>
                    </div>
                  </div>
                </div>
                <input type="submit" value="Submit" id="sent_data" hidden="hidden">
            </div>
          </div>
        </div>

      </div>

    </div>

    <!-- CH 2 -->
    <div class="tab-pane active" id="link1" aria-expanded="true">
      <div class="col-md-12 mb-4 fadeIn" style = "padding:0px !important;">
        
        <div class="card">
        <div href="#CH2" class="card-header waves-effect" data-toggle="collapse" id="GCH2"><i class="fas fa-chart-line  fa-2x mr-2"></i><B id = "show_time_channel_2">CH 2</B></div>
          <div class="card-body collapse" id="CH2">
            <div id="ch2" style="/*height: 400px;*/ min-width: 310px">
                <input type="checkbox" name="index" value="" id="index" checked style="display:none;">
                <div class="col-md-12 mb-4 fadeIn">
                  <div class="container-fluid ">
                    <div class="card-body collapse" id="CH2">
                      <div id="ch2" style="height: 400px; min-width: 310px"></div>
                    </div>
                  </div>
                </div>
                <input type="submit" value="Submit" id="sent_data" hidden="hidden">
            </div>
          </div>
        </div>

      </div>

    </div>

    <!-- CH 3 -->
    <div class="tab-pane active" id="link1" aria-expanded="true">
      <div class="col-md-12 mb-4 fadeIn" style = "padding:0px !important;">
        
        <div class="card">
        <div href="#CH3" class="card-header waves-effect" data-toggle="collapse" id="GCH3"><i class="fas fa-chart-line  fa-2x mr-2"></i><B id = "show_time_channel_3">CH 3</B></div>
          <div class="card-body collapse" id="CH3">
            <div id="ch3" style="/*height: 400px;*/ min-width: 310px">
                <input type="checkbox" name="index" value="" id="index" checked style="display:none;">
                <div class="col-md-12 mb-4 fadeIn">
                  <div class="container-fluid ">
                    <div class="card-body collapse" id="CH3">
                      <div id="ch3" style="height: 400px; min-width: 310px"></div>
                    </div>
                  </div>
                </div>
                <input type="submit" value="Submit" id="sent_data" hidden="hidden">
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>
</main>
<!--Main layout-->

<!--Footer-->
<footer class="page-footer text-center font-small primary-color-dark darken-2 mt-4" style = "visibility: visible !important;background-color: #cbefff !important;">
  <!--Copyright-->
  <div class="footer-copyright py-2">
    © 2019 Copyright:
    <a href="http://www.pttep.com/en/index.aspx" target="_blank"> pttep.com </a>
  </div>
  <!--/.Copyright-->

</footer>
<!-- change time zone -->
<div class="modal fade" id="change_datetime_show" tabindex="-1" role="dialog" aria-labelledby="change_datetime_show_ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="change_datetime_show_ModalLabel">Change Time Zone</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <br>
                <div class='row'>
                    <div class = 'col-12'>
                        SELECT TIME ZONE:
                        <select id="select_timezone" class="browser-default custom-select">
                            <option selected="" value="0">SELECT TIME ZONE</option>
                            <option value="1">UTC</option>
                            <option value="2">GMT (Thailand)</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button onclick = "get_timezone()"id = "modal_confirm_add_device_to_event" type="button" class="button-save">Save</button>
                <button id = "modal_close" type="button" class="button-close " data-dismiss="modal">Close</button>
            </div>
      </div>
    </div>
</div>
<!--/.Footer-->

<!-- SCRIPTS -->

<!-- JQuery -->
<script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="../js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.js"></script>
<!-- fontawesome -->
<script type="text/javascript" src="../font/fontawesome/js/all.min.js"></script>
<!-- datetimepicker -->
<script type="text/javascript" src="../js/jquery.datetimepicker.full.min.js"></script>
<!-- dataTables -->
<script type="text/javascript" src="../js/datatables.js"></script>
<!-- Moment -->
<script type="text/javascript" src="../js/moment.js"></script>
<!-- Google Maps -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDZua0o5Tg5lNTmzsUdgAQkdMYk7yvGIVo&callback=initMap" async defer></script> -->
<!-- Highstock -->
<script type="text/javascript" src="../js/addons/Highstock.js"></script>
<script type="text/javascript" src="../js/modules/exporting.js"></script>
<script type="text/javascript" src="../js/modules/export-data.js"></script>

<!-- JS Function -->
<script type="text/javascript" src="../js/function/page1.js"></script>
<script type="text/javascript" src="../js/function/page2.js"></script>
<script type="text/javascript" src="../js/function/page3.js"></script>
<script type="text/javascript" src="../js/function/option.js"></script>



<!-- this is axios-master github lib  -->
<!-- <script src="./../axios/axios-master/dist/axios.min.js"></script> -->







<!-- Initializations -->
<script type="text/javascript">
var device_arr = [];
var strMonthCut = ["ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."];
var graph_arr=[];
var map,table_s,marker;
var device_status,device_id="WS000001";
let socket_data = new WebSocket("ws://128.199.218.27:1880/ptt");
let time = 0;
var param_get_timezone = <?php echo $_GET['timezone'];?>;
$( document ).ready(function() {



  // set_select_option();
  cook_is_cook()


var serin1 = setInterval(function(){   
  // 128.199.218.27:3000/api/data/last?id=WS000001
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://128.199.218.27:3000/api/data/last?id=" + JSON.parse('<?php echo $_GET['device'];?>')[0],
    "method": "GET",
  }

  $.ajax(settings).done(function (event) {
    console.log(event)
    // var obj = JSON.parse(event);
    // console.log(obj);
    var device_id_get_php = '<?php echo $_GET['device'];?>'
    // console.log(device_id_get_php)
    get_realtime(event,device_id_get_php);
    time = 0;
  });
}, 1000);

  view_view_device();
  socket_data.onopen = function(e) {
    console.log("[open] Connection established, send -> server");

    // var device = getCookie("device_now");
    // var device_obj = JSON.parse(device)
    // socket_data.send(JSON.parse('<?php echo $_GET['device'];?>')[0]);
    // console.log(JSON.parse('<?php echo $_GET['device'];?>')[0])
  };

  socket_data.onmessage = function(event) {

    // console.log(event.data);
    // var obj = JSON.parse(event.data);
    // console.log(obj);
    // var device_id_get_php = '<?php echo $_GET['device'];?>'
    // console.log(device_id_get_php)
    // get_realtime(obj,device_id_get_php);
    // time = 0;

  };

  socket_data.onclose = function(event) {
  if (event.wasClean) {
      // alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
      // location.reload();
  } else {
      // alert('[close] Connection died');
      // location.reload();
  }
  };

  socket_data.onerror = function(error) {
      // alert(`[error] ${error.message}`);
      // location.reload();
  };
  open_dashboard();
  delete_device();
  modal_close();
  modal_confirm();
  record_data('<?PHP echo $_GET['device'];?>' );
  // stop_data();
  logout();
  console.log("ready!");
  // starting-----------------------------------------

  $('#start_date_row').val(moment().format('YYYY-MM-DD HH:mm:ss'));
  $('#end_date_row').val(moment().format('YYYY-MM-DD HH:mm:ss'));
  // $('#start_date_row').val(moment().format('2019-02-05 12:11:53')); //test
  // $('#end_date_row').val(moment().format('2019-02-05 12:11:54')); //test

  try {
    // initMap();
    var serin = setInterval(function(){   check_timeout(); }, 1000);
    input_date();
    btn_start();
    new WOW().init();
  } catch (e) {
    location.reload();
  }

  // page 1-------------------------------------------
  var serin;
  batt(0);

  // page 2-------------------------------------------
  // get_row(time_set($('#start_date_row').val()),time_set($('#end_date_row').val()));

  // page 3-------------------------------------------

  get_device();

  // option-------------------------------------------
  real_date();

  // test---------------------------------------------
  console.log(	new Date().getTime() );

});

// starting
// function initMap() {
//   map = new google.maps.Map(document.getElementById('map'), {
//     center: {lat: 13.8557611, lng: 100.4783308},
//     zoom: 8,
//     mapTypeId:  google.maps.MapTypeId.HYBRID
//   });
// }
function check_timeout(){
  time += 1;
  console.log(time);
  if (time == 10){
    get_realtime(
      [
        {
          "id": "WS20180001",
          "ch_count":1,
          "UTC": 1564432500000,
          "Time": "2018-12-24T02:51:37.248Z",
          "sampling": 1000,
          "payload": [
                        {
                          "ch": "1",
                          "data": []
                        },
                        {
                          "ch": "2",
                          "data": []
                        },{
                          "ch": "3",
                          "data": [ ]
                        }
                      ]
        }
      ]
    );
  }
}

function get_device() {
  console.log('..get_device..');


  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://128.199.218.27:3000/api/device",
    "method": "GET"
  }

  $.ajax(settings).done(function (response) {
    console.log(response + "OK");
    var val=response;
    $("#index").val(val.length);
    for(var i=0;i<val.length;i++){
      var deferent = (Math.floor(new Date().getTime()/1000.0 - new Date(val[i].timestamp).getTime()/1000.0));
      var content_On_off = ""
      if (deferent <= 60){
        content_On_off = '<button type="button" class="btn btn-outline-success btn-sm m-0 waves-effect"><B>ONLINE</B></button>';
      }else{
        content_On_off = '<button type="button" class="btn btn-outline-danger btn-sm m-0 waves-effect"><B>OFFNLINE</B></button>';
      }
      var show_data_time;
      if (!isNaN(new Date(val[i].timestamp).getHours()) || !isNaN(new Date(val[i].timestamp).getDate())){
        show_data_time = '<td id = "show_time_timestamp'+val[i].id+'" >วันที่ ' + new Date(val[0].timestamp).getDate() +" เดือน "+strMonthCut[new Date(val[0].timestamp).getMonth()]+ ' '+ (new Date(val[0].timestamp).getFullYear() + 543) + ' เวลา : ' + new Date(val[i].timestamp).getHours()+":"+new Date(val[i].timestamp).getMinutes()+":"+new Date(val[i].timestamp).getSeconds()+" น."+'</td>'
      }else{
        show_data_time = '<td id = "show_time_timestamp'+val[i].id+'" >วันที่ - เดือน - เวลา : -:-:- น.</td>'; 
      }
      $("#device_table_body").append(
        '<tr>'+
          '<td>'+
            '<div class="custom-control custom-checkbox">'+
              '<input type="checkbox" class="custom-control-input" id="checked'+i+'" name="checked'+i+'" value="'+val[i].id+'">'+
              '<label class="custom-control-label" for="checked'+i+'"></label>'+
            '</div>'+
          '</td>'+
          // '<td>'+(i+1)+'</td>'+
          '<td>'+val[i].id+'</td>'+
          '<td>device_</td>'+
          '<td>'+val[i].lat+'</td>'+
          '<td>'+val[i].long+'</td>'+
          show_data_time+
          // '<td>'+val[i].port+'</td>'+ 
          '<td id = "content_On_off'+val[i].id+'">'+
            content_On_off+
          '</td>'+
          '<td id = "stream'+val[i].id+'">'+ 
            ((val[i].stream == 0) ? '<button type="button" class="btn btn-outline-danger btn-sm m-0 waves-effect"><B>INACTIVE</B></button>' : '<button type="button" class="btn btn-outline-success btn-sm m-0 waves-effect"><B>ACTIVE</B></button>')+
          '</td>'+
        '</tr>'
      );
    }

    var table_d = $("#device_table").DataTable({
      scrollY: "500px",
      paging: true,
      destroy: true,
      dom: 'Bfrtip',
      lengthChange: true,
      buttons: [
        {
          extend: 'csv',
          text: 'EXPORT CSV',
          exportOptions: {
            modifier: {
              search: 'none'
            }
          }
        }
      ]
    });
    init_menu_config(val[0]);
  });
}

function input_date() {

  // select date RAW
  $('#start_date_row').datetimepicker({
    timepicker:true,
    format:'Y-m-d H:i:s'
  });
  $('#end_date_row').datetimepicker({
    timepicker:true,
    format:'Y-m-d H:i:s'
  });

  // select date Sise nav
  // $('#start_date').datetimepicker({
  //   timepicker:false,
  //   format:'d-m-Y'
  // });
  // $('#end_date').datetimepicker({
  //   timepicker:false,
  //   format:'d-m-Y'
  // });

}

function btn_start() {
  // page 1
  $('#GCH1').click();
  $('#GCH2').click();
  $('#GCH3').click();
  $('#map_box').click();
  $('#map_box1').click();
  $('#batt_box').click();
  $('#temp_box').click();
  $('#temper').text(30);

  // page 2
  $('#GROW').click();


}



function logout(){
	// Submit
	try {
    $("#logout").click( function (){
      window.location.replace("../index.html");
    });
	} catch (e) {
		console.log(e);
	}

}


function delete_device(){
	try {
    $("#delete_device").click( function (){
      console.log(this);
    });
	} catch (e) {
		console.log(e);
	}

}

function modal_close(){
	try {
    $("#modal_close").click( function (){
      $("#text_device").val("") ;
    });
	} catch (e) {
		console.log(e);
	}

}

function modal_confirm(){
	try {
    $("#modal_confirm").click( function (){
      console.log('{"device":{"id":"'+$("#text_device").val()+'"}}');
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://128.199.218.27:3000/api/device/add",
        "method": "POST",
        "headers": {
          "Content-Type": "application/json",
          "X-Requested-With": "XMLHttpRequest",
          "Authorization": "Token <?php echo $_SESSION["token"];?>"
        },
        "processData": false,
        "data": '{"device":{"id":"'+$("#text_device").val()+'"}}'
      }
      $.ajax(settings).done(function (response) {
        console.log(response);
        alert("INPUT NEW DEVICE SUCCESS.");
        location.reload();
      });
      $('#exampleModal').modal('hide')
      $("#text_device").val("")
    });
  } catch (e) {
    console.log(e);
  }
}


function record_data(device_ID){
	try {
    // serin = setInterval(function(){   get_realtime(device_ID); }, 1000);
	} catch (e) {
		console.log(e);
	}
}

function stop_data(){
	try {
    clearInterval(serin);
	} catch (e) {
		console.log(e);
	}
}
function init_menu_config(val){
  //var val=response.reverse();
  var deferent = (Math.floor(new Date().getTime()/1000.0 - new Date(val.timestamp).getTime()/1000.0));
  $('#side_id').text(val.id);
  if (deferent <= 60){
    $('#side_status').text("ONLINE");
  }else{
    $('#side_status').text("OFFNLINE");
  }
}
function open_dashboard(){
  $('#open_dashobard').click(function() {
    // window.location.href = './dashboard.php';
    window.open('./view_chart.php', '_blank');
  });
}
function set_select_option(){
  var content_out_stream = "";
  content_out_stream += "<option selected value = '0' selected>SELECT DEIVCE ID</option>"
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://128.199.218.27:3000/api/device",
    "method": "GET"
  }

  $.ajax(settings).done(function (response) {
    var val=response;
    console.log(val);
    device_arr = val;
    for (var i = 0;i< val.length;i++){
      content_out_stream += "<option value = '"+(i+1)+"'>"+val[i].id+"</option>"
    }
    // content_out_stream += "<option value = '' selected>SELECT DEIVCE ID</option>"
    $("#SELECT_DEVICE_ID").html("");
    $('#SELECT_DEVICE_ID').append(content_out_stream);  

  });
}
function view_view_device(){
  $("#view_device").on('click',function(){
    if ($("#SELECT_DEVICE_ID").val() == 0){
      alert("Please select DEVICE_ID")
    }else{
      // alert(device_arr[$("#SELECT_DEVICE_ID").val() - 1].id)

      var device = getCookie("device_now");
      var device_obj = JSON.parse(device)
      window.location.replace("../source/view_chart.php?device=[\"" + device_obj[$("#SELECT_DEVICE_ID").val() - 1] + "\"]&timezone=<?php echo $_GET['timezone'];?>");
    }
  })
}




//// is cook

function cook_is_cook(){
  var content_out_stream = "";
  content_out_stream += "<option selected value = '0' selected>SELECT DEIVCE ID</option>"
  var val = JSON.parse(getCookie('device_now'))
  for (var i = 0;i< val.length;i++){
    if (val[i] != null){
      content_out_stream += "<option value = '"+(i+1)+"'>"+val[i]+"</option>"
    }
  }
  // content_out_stream += "<option value = '' selected>SELECT DEIVCE ID</option>"
  $("#SELECT_DEVICE_ID").html("");
  $('#SELECT_DEVICE_ID').append(content_out_stream); 
}


function setCookie(cname,cvalue,exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires=" + d.toGMTString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "[]";
}

function checkCookie() {
  var user=getCookie("username");
  if (user != "") {
    alert("Welcome again " + user);
  } else {
     user = prompt("Please enter your name:","");
     if (user != "" && user != null) {
      //  setCookie("username", user, 30);
     }
  }
}






// cook is cook

function setCookie(cname,cvalue,exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires=" + d.toGMTString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "[]";
}

function checkCookie() {
  var user=getCookie("username");
  if (user != "") {
    alert("Welcome again " + user);
  } else {
     user = prompt("Please enter your name:","");
     if (user != "" && user != null) {
      //  setCookie("username", user, 30);
     }
  }
}
function get_timezone(){
  // 1 = UTC
  // 2 = GMT
  if ($("#select_timezone").val() != "0"){
    console.log($("#select_timezone").val());
    switch($("#select_timezone").val()) {
      case "1":
        windown_get_param("0")
        break;
      case "2":
        windown_get_param("7")
        break;
      default:
        console.log("not OK not")
    }
  }else{
    alert("Please select Timezone")
  }
}
function windown_get_param(value){
  var url = new URL(window.location.href);
  url.searchParams.set('timezone',value);
  window.location.href = url.href;
}
</script>
</body>
</html>
