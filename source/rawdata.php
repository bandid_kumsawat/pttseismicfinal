<?php
  session_start();
  if(isset($_SESSION['token'])) {
      $token = $_SESSION['token'];
  } else {
    header("Location: ../index.html");
    exit;
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <link rel="icon" href="../img/logo/logo-pttep.png" type="image/png" sizes="17x17">
  <title>WIRELESS LAND SEISMIC MONITORING</title>
  <!-- Font Awesome -->
  <link href="../font/fontawesome/css/all.min.css" rel="stylesheet">
  <!-- Bootstrap core CSS -->
  <link href="../css/bootstrap.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="../css/mdb.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="../css/style.min.css" rel="stylesheet">
  <!-- Your custom datetimepicker (optional) -->
  <link href="../css/jquery.datetimepicker.min.css" rel="stylesheet">
  <!-- Your custom dataTable (optional) -->
  <link href="../css/datatables.css" rel="stylesheet">

  
  <style>
  .persent-persent{
    text-align: right;
  }
  #myProgress {
    width: 100%;
    background-color: #ddd;
  }

  #myBar {
    width: 1%;
    height: 30px;
    background-color: #4CAF50;
    text-align: left;
  }
  .scroll-bar-table{
    height: 300px;
    overflow: scroll;
  }
  .map-container{
    overflow:hidden;
    padding-bottom:56.25%;
    position:relative;
    height:0;
  }
  .map-container iframe{
    left:0;
    top:0;
    height:100%;
    width:100%;
    position:absolute;
  }

  .tr_color {color: #F5F5F5;}

  .mouse-hover{
    cursor: pointer;
  }

  .bg-device-color{
    background-color: #ffffff;
    color:#000000;
  }
  .buttons-csv{
    width: 120px;
    position:absolute !important;
    top:6px;

  }
/* 
p.ex1 {display: none;}
p.ex2 {display: inline;}
p.ex3 {display: block;}
p.ex4 {display: inline-block;} 
*/
  #show_side_hide{
    /* display: none; */
    display: block;
    padding: 0px 30px 0px 30px;
    width:40%;
  }
  #show_image_seimic{
    display: inline-block;
    padding: 0px 30px 0px 30px;
    width:40%;
  }
  @media (max-width: 1199.98px) {
    #show_side_hide {
      padding: 0px 30px 0px 30px;
      display: block;
    } 
  }
  #content_1{
    padding-left:25px;
  }
  
  /* new nav active */
  .nav-active{
    border-bottom:6px solid #000000;
    border-radius: 3px;
  }
  .nav-pills .nav-link.active,
  .nav-pills .show > .nav-link {
    border-bottom: 5px solid #007bff;
  }
  .nav-menu {
    color: #007bff;
  }

  .dropdown-menu {
    left: auto;
    right: 0;
    float: right;
  }
  .nav-pills .nav-link {
    border-radius: 0rem;
  }
  .bg-image {
    background-image: url(../login/images/BG_09.jpg);
  }
  .image-image-image{
    /* background-image: url(../login/images/522343-earthquake.jpg); */
  }
  </style>
</head>

<body class=" lighten-3">

  <!--Main Navigation-->
  <header>

    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
      <div class="container-fluid">

        <!-- Collapse -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Links -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

          <!-- Left -->
          <ul class="nav nav-pills nav-pills-primary mr-auto" role="tablist">
            <li class="nav-item" style = "padding-right: 12px;">
              <a class="nav-link" style = "padding-bottom: 0px !important;padding-right: 4px !important;padding-left:4px !important;" href="./dashboard.php?timezone=<?php echo $_GET['timezone']; ?>" role="tablist" aria-expanded="true">
                <i class="fas fa-tram mr-1"></i> <B>DEVICE MANAGER</B>
              </a>
            </li>
            <!-- <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#link1" role="tablist" aria-expanded="false">
                <i class="fas fa-chart-bar mr-1"></i> <B>DASHBOARD</B>
              </a>
            </li> -->

            <li class="nav-item " style = "padding-right: 12px;">
              <a class="nav-link" style = "padding-bottom: 0px !important;padding-right: 4px !important;padding-left:4px !important;" href='#' onclick = "link_fc_read_time_page();" role="tablist" aria-expanded="false">
              <!-- ?token=<?php echo $_SESSION['token'];?>&email=<?php echo $_SESSION["email"]; ?> ./view_chart.php?device=["WS000001"]          --> 
                <i class="fas fa-chart-line mr-1"></i> <B>REAL TIME</B>
              </a>
            </li>

            <li class="nav-item" style = "padding-right: 12px;">
              <a class="nav-link active" style = "padding-bottom: 0px !important;padding-right: 4px !important;padding-left:4px !important;" href="./rawdata.php?timezone=<?php echo $_GET['timezone']; ?>" role="tablist" aria-expanded="false">
                <i class="fas fa-table mr-1"></i><B>DAQ DATA</B>
              </a>
            </li>


            <!--<li class="nav-item">
              <button  style = "width: 100%;" type="button" class="btn btn-info btn-sm" id="open_dashobard">View Dashboard</button>
            </li>-->


          </ul>

          <!-- Right -->
          <ul class="nav nav-menu">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown">
                <i class="fas fa-address-card mr-1"></i><B><?php echo $_SESSION['email'] ?></B>
              </a>
              <div class="dropdown-menu">
                <center>
                 <B><?php echo $_SESSION['email'] ?></B>
                  <img src = "../login/images/admin.png" width="100px;"/>
                  <a class="dropdown-item">
                    <button id = "c_time"class='btn btn-outline-info btn-sm' data-toggle="modal" data-target="#change_datetime_show">Change Time</button><br>
                    <button id = "logout"class='btn btn-outline-danger btn-sm'>Logout</button>
                </a>
              </center>
              </div>
            </li>
          </ul>

        </div>

      </div>
    </nav>
    <!-- Navbar -->

    <!-- Sidebar -->
    <div class="sidebar-fixed position-fixed bg-image">

      <!-- <a class="logo-wrapper waves-effect"> -->
      <div class="mb-4">
        <center><img src="../img/logo/logo-pttep.png"  alt="" style="width: 70%; height: 70%"></center>
      </div>
      <!-- </a> -->

      <!-- <M>Menu Config</M> -->
      <div class="mb-2"></div>

      <table class="table table-hover">
        <tbody>
          <tr>
            <th>Project</th>
            <td>WIRELESS LAND SEISMIC</td>
          </tr>
          <tr>
            <th>Date</th>
            <td id="real_date"></td>
          </tr>
          <!-- <tr>
            <th>Status</th>
            <td id="side_status"></td>
          </tr> -->
    </tbody>
  </table>

  <div class='row'>
    <div class='col-sm-12'>
      <!-- <button  style = "width: 100%;" type="button" class="btn btn-info btn-sm" id="open_dashobard">View Dashboard</button> -->
    </div>
  </div>
</div>
<!-- Sidebar -->

</header>
<!--Main Navigation-->

<!--Main layout-->
<main class="pt-5 mx-lg-5">
  <div class="container-fluid mt-5">
    <div class="tab-content tab-space">
    <!-- Page 2 -->
    <div class="tab-pane active" id="link1" aria-expanded="true">
      <!-- Graph RAW -->
      <div class="col-md-12 mb-4 fadeIn">
        <div class="row">
          <!-- <div class="col-md-2"></div> -->


          <div class="col-md-12  fadeIn">
            <div class="card ">
              <div class="container">
                <div class="col-12">
                  <div class="row">
                    <div class='col-12'>
                      <center>
                        <p><div style = "color: black;font-size: 1.5em;font-weight: 700;">
                        <?php 
                          if ($_GET['timezone'] == "0"){
                            echo "Time Zone [ UTC ]";
                          }else if ($_GET['timezone'] == '7') {
                            echo "Time Zone [ GMT Thailand ]";
                          }
                        ?>
                      </div></p>
                    </center>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <br><br>


          
          <div class="col-md-12  fadeIn">
            <div class="card ">
              <div class="container">
                <div class="col-12">
                  <div class="row">

                    <div class="col-3">
                      <p class="mt-2">SELECT DEVICE</p>
                      <div class="input-group mt-3 mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fas fa-microchip"></i><!-- <i class="fas fa-calendar-week"></i> --></span>
                        </div>
                        <select id = 'SELECT_DEVICE_ID' class="browser-default custom-select">
                          <option selected>SELECT DEIVCE ID</option>
                          <option value="1">One</option>
                          <option value="2">Two</option>
                          <option value="3">Three</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-3">
                      <p class="mt-2">START DATE</p>
                      <div class="input-group mt-3 mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fas fa-calendar-week"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Start Date" aria-label="Start_Date" id="start_date_row" value="">
                      </div>
                    </div>
                    <div class="col-3">
                      <p class="mt-2">END DATE</p>
                      <div class="input-group mt-3 mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fas fa-calendar-week"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="End Date" aria-label="End_Date" id="end_date_row" value="">
                      </div>
                    </div>

                    <!-- period -->
                    <div class="col-2">
                      <p class="mt-2">SELECT PERIOD</p>
                        <div class="input-group mt-3 mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-hand-pointer"></i><!-- <i class="fas fa-calendar-week"></i> --></span>
                          </div>
                          <select id = 'SELECT_PERIOD' class="browser-default custom-select">
                            <option selected value="5">5 sec.</option>
                            <option value="10">10 sec.</option>
                            <option value="20">20 sec.</option>
                          </select>
                        </div>
                    </div>


                    <div class="col-1">
                      <div class="mt-4 mb-3">
                        <button type="button" class="btn blue-gradient btn-rounded" id="search">search</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>


      <h2>DAQ Table</h2>
      <hr>
      <!-- start export all  -->
      <div class='container-fluid'>
        <div class='row'>
        <div class='col-sm-12'>EXPORT CSV</div>
          <div class='col-sm-6'>
            <div class='row'>
              <div class='col-sm-12 scroll-bar-table'>
                <table class="table table-hover table-bordered">
                  <thead>
                    <tr class="bg-device-color" align="center">
                      <th scope="col" style = "font-size: 1rem;font-weight:900;"width="100%">Selected Device ID</th>
                    </tr>
                  </thead>
                  <tbody align="center" style = "font-size: 1rem !important;font-weight:500 !important;" id = "select_device_show">

                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <!-- <div class="col-3">
              <div class="input-group mt-3 mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fas fa-calendar-week"></i></span>
                </div>
                <input type="text" class="form-control" placeholder="Start Date" aria-label="Start_Date" id="start_date_row_export" value="">
              </div>
            </div>
            <div class="col-3">
              <div class="input-group mt-3 mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="fas fa-calendar-week"></i></span>
                </div>
                <input type="text" class="form-control" placeholder="End Date" aria-label="End_Date" id="end_date_row_export" value="">
              </div>
            </div>
            <div class='col-3'>
              <button class='btn blue-gradient btn-rounded waves-effect waves-light' id = "export_all_all_all">EXPORT FILE</button>
            </div> -->


            
          <div class='col-sm-6'>
            <div id="myProgress">
              <div id="myBar"></div>
            </div>
            <div class='persent-persent' id = 'persent'>0%</div>
            <div class='row'>
              <div class='col-6 input-group mt-2 mb-2'>
                <button class='btn blue-gradient btn-rounded waves-effect waves-light' style = "width: 500px !important;" id = "export_all_all_all">GENERATE .CSV FILE</button>
              </div>
              <div class='col-6 input-group mt-2 mb-2'>
                <button class='btn blue-gradient btn-rounded waves-effect waves-light' style = "width: 500px !important;" id = "export_all_all_all_zip">Download .Zip</button>
              </div>
            </div>
          </div>




        </div>
      </div>
      <br>
      <!-- end export all -->
      <div hidden class = "row">

        <div class="col-2">
          <div class="mt-4 mb-3">
            <!-- <button type="button" class="btn blue-gradient btn-rounded" id="export_data">EXPORT</button> -->
            <button hidden type="button" class="btn blue-gradient btn-rounded"  data-toggle="modal" data-target="#exampleModal_waitting" id="export_data_all">EXPORT ALL</button> 
            <!-- <i id = "on_load_wait_" class='fa fa-spinner fa-spin' style = "font-size: 20px;"></i> -->
            <i id = "on_load_wait_"></i>
          </div>
        </div>

        <div class="col-2">
          <div class="mt-4 mb-3">
            <!-- <button type="button" class="btn blue-gradient btn-rounded" id="export_data_all">EXPORT ALL</button> -->
            <!-- <i id = "on_load_wait" class='fa fa-spinner fa-spin' style = "font-size: 20px;"></i> -->
          </div>
        </div>

      </div>


      <div class="head_table table-responsive">



        <!-- <table class="table table-hover table-bordered" id="row_table">
          <thead>
            <tr class="bg-primary tr_color">
              <th>ID Device</th>
              <th>Channel</th>
              <th>UTC</th>
              <th>TIME</th>
              <th>Payload</th>
            </tr>
          </thead>
          <tbody id="table_body">

          </tbody>
        </table> -->



        <table class="table table-hover table-bordered" id="row_table">
          <thead>
            <tr class="bg-primary tr_color">
              <th>#</th>
              <th>Start Time</th>
              <th>End Time</th>
              <th hidden></th>
              <th hidden></th>
              <th hidden></th>
              <th hidden></th>
              <th hidden></th>
              <!-- <th>ดาวน์โหลดไฟล์</th> -->
            </tr>
          </thead>
          <tbody id="table_body">

          </tbody>
        </table>



        <!-- <center><div class="loader" id="row_load2"></div></center> -->




      </div>


      <div class="col-md-12 mb-4 fadeIn" style = "padding-top: 20px !important;">
        <div class="card">
          <div href="#row_coll" class="card-header waves-effect" data-toggle="collapse" id="GROW">
          <i class="fas fa-chart-line fa-2x mr-2"></i><B>Graph</B></div>

          <div class="card-body collapse" id="row_coll">
            <div id="ch3" style="height: 400px; min-width: 310px"></div>
          </div>
        </div>

      </div>


    </div>


  </div>
</main>
<!--Main layout-->



                      <div class="modal fade" id="exampleModal_waitting" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel_waitting" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel_waitting">Please wait preparing file .csv</h5>
                                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button> -->
                            </div>
                              <div class="modal-body">
                                <br>

                                <div class='row'>
                                  <div class = 'col-12'>
                                    <center><i id = "on_load_wait" class='fa fa-spinner fa-spin' style = "font-size: 60px;"></i></center>
                                  </div>
                                </div>
                              </div>
                          </div>
                        </div>
                      </div>



<!--Footer-->
<footer class="page-footer text-center font-small primary-color-dark darken-2 mt-4" style = "visibility: visible !important;background-color: #cbefff !important;">
  <!--Copyright-->
  <div class="footer-copyright py-2">
    © 2019 Copyright:
    <a href="http://www.pttep.com/en/index.aspx" target="_blank"> pttep.com </a>
  </div>
  <!--/.Copyright-->

</footer>
<!-- change time zone -->
<div class="modal fade" id="change_datetime_show" tabindex="-1" role="dialog" aria-labelledby="change_datetime_show_ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="change_datetime_show_ModalLabel">Change Time Zone</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <br>
                <div class='row'>
                    <div class = 'col-12'>
                        SELECT TIME ZONE:
                        <select id="select_timezone" class="browser-default custom-select">
                            <option selected="" value="0">SELECT TIME ZONE</option>
                            <option value="1">UTC</option>
                            <option value="2">GMT (Thailand)</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button onclick = "get_timezone()"id = "modal_confirm_add_device_to_event" type="button" class="button-save">Save</button>
                <button id = "modal_close" type="button" class="button-close " data-dismiss="modal">Close</button>
            </div>
      </div>
    </div>
</div>
<!--/.Footer-->

<!-- SCRIPTS -->

<!-- JQuery -->
<script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="../js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.js"></script>
<!-- fontawesome -->
<script type="text/javascript" src="../font/fontawesome/js/all.min.js"></script>
<!-- datetimepicker -->
<script type="text/javascript" src="../js/jquery.datetimepicker.full.min.js"></script>
<!-- dataTables -->
<script type="text/javascript" src="../js/datatables.js"></script>
<!-- Moment -->
<script type="text/javascript" src="../js/moment.js"></script>
<!-- Google Maps -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDZua0o5Tg5lNTmzsUdgAQkdMYk7yvGIVo&callback=initMap" async defer></script> -->
<!-- Highstock -->
<script type="text/javascript" src="../js/addons/Highstock.js"></script>
<script type="text/javascript" src="../js/modules/exporting.js"></script>
<script type="text/javascript" src="../js/modules/export-data.js"></script>

<!-- JS Function -->
<script type="text/javascript" src="../js/function/page1.js"></script>
<script type="text/javascript" src="../js/function/page2.js"></script>
<script type="text/javascript" src="../js/function/page3.js"></script>
<script type="text/javascript" src="../js/function/option.js"></script>

<script type="text/javascript" src="../js/addons/boost.js"></script>

<!-- this is axios-master github lib  -->
<script src="./../axios/axios-master/dist/axios.min.js"></script>









<!-- Initializations -->
<script type="text/javascript">
var strMonthCut = ["ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."];
var graph_arr=[];
var map,table_s,marker;
var device_arr = [];
var device_status,device_id="WS000001";
var data_table_json
var marker, i, info,content_in_map = [];
var interval_variable_global;
var param_get_timezone = <?php echo $_GET['timezone'];?>;

$( document ).ready(function() {
  $("#export_all_all_all_zip").attr("disabled", "disabled"); 


  set_select_option();
  open_dashboard();
  modal_close();
  modal_confirm();
  // record_data();
  // stop_data();
  //init_menu_config();
  // setInterval(function(){ check_status();  }, 5*1000);
  delete_device();
  logout();
  //console.log("ready!");
  // starting-----------------------------------------

  // $('#start_date_row').val(moment().format('YYYY-MM-DD HH:mm:ss'));
  // $('#end_date_row').val(moment().format('YYYY-MM-DD HH:mm:ss'));
  //$('#start_date_row').val(moment().format('2019-11-07 23:10:00')); //test
  //$('#end_date_row').val(moment().format('2019-11-07 23:12:00')); //test

  $('#start_date_row_export').val(moment().format('2019-11-07 23:10:00')); //test
  $('#end_date_row_export').val(moment().format('2019-11-07 23:12:00')); //test
  if (param_get_timezone == 0){
    $('#start_date_row').val(moment().add(-7,"hours").format('YYYY-MM-DD HH:mm:ss'));
    $('#end_date_row').val(moment().add(-7,"hours").format('YYYY-MM-DD HH:mm:ss'));

    // $('#start_date_row').val(moment($("#start_date_row").val(),'YYYY-MM-DD HH:mm:ss:SSS').add(-7,'hours').format('YYYY-MM-DD HH:mm:ss'));
    // $('#end_date_row').val(moment($("#start_date_row").val(),'YYYY-MM-DD HH:mm:ss:SSS','YYYY-MM-DD HH:mm:ss:SSS').add(-7,'hours').format('YYYY-MM-DD HH:mm:ss'));
  }else if (param_get_timezone == 7){
    $('#start_date_row').val(moment().format('YYYY-MM-DD HH:mm:ss'));
    $('#end_date_row').val(moment().format('YYYY-MM-DD HH:mm:ss'));
  }



  try {
    // initMap();
    input_date();
    btn_start();
    new WOW().init();
  } catch (e) {
    location.reload();
  }

  // page 1-------------------------------------------
  var serin;
  batt(0);

  // page 3-------------------------------------------

  // get_device();

  // option-------------------------------------------
  real_date();

  // test---------------------------------------------
  //console.log(	new Date().getTime() );


  // page 2-------------------------------------------
  device_id="WS000001"
  get_row(($('#start_date_row').val()),($('#end_date_row').val()),($('#SELECT_PERIOD').val()));

  $("#export_data_all").on("click",function(){

    $("#on_load_wait").show();
    $("#on_load_wait_").show();


    var table = $("#table_body");
    var trs = table.find('tr');
    var device = trs.eq(0).find('td').eq(3).text();
    var end_date = $("#end_date_row").val().split(" ")[0]+"T"+$("#end_date_row").val().split(" ")[1] + "";
    var start_date = $("#start_date_row").val().split(" ")[0]+"T"+$("#start_date_row").val().split(" ")[1]+"";
    var url = "http://128.199.218.27:3001/api/data/export?id=" + device +"&st=" + start_date + "&ed=" + end_date
    // http://128.199.218.27:3001/api/data/export?id=WS000001&st=2019-10-30T10:00:00&ed=2019-10-30T15:00:00



    // window.open(url)


    var settings = {
      "async": true,
      "crossDomain": true,
      "url": url,
      "method": "GET",
      "timeout":30000000
    }
    //console.log(settings);
    $.ajax(settings).done(function (response) {
      //console.log(response);
      // $("#on_load_wait_").hide();
      // $("#exampleModal_waitting").modal('hide')
      // window.open("http://ssmonitors.site/"+response.url)

      setTimeout(function(){ 
        $("#on_load_wait_").hide();
        $("#exampleModal_waitting").modal('hide')
        window.open("http://ssmonitors.site/"+response.url)

        // const a = document.createElement("a");
        // a.style.display = "none";
        // document.body.appendChild(a);
        // a.href = "http://ssmonitors.site/"+response.url
        // a.click();
      }, 10000);

    });



    // try{
    //   let promise = new Promise((resolve, reject) => {
    //     //console.log(trs.eq(i).find('td').eq(3).text())
    //     $("#on_load_wait").show();
    //     var settings = {
    //       "async": true,
    //       "crossDomain": true,
    //       "url": "http://128.199.218.27:3000/api/data/export?id="+trs.eq(0).find('td').eq(3).text()+"&st="+$("#start_date_row").val().split(" ")[0]+"T"+$("#start_date_row").val().split(" ")[1]+"0"+"&ed=" + $("#end_date_row").val().split(" ")[0]+"T"+$("#end_date_row").val().split(" ")[1] + "0", 
    //       "method": "GET",
    //       "timeout":30000000
    //     }     
    //     //console.log(settings);
    //     $.ajax(settings).done(function (response) {
    //       // downloadFile(response,trs.eq(i).find('td').eq(3).text()+"__"+trs.eq(i).find('td').eq(1).text().replace(" ","T")+"__"+trs.eq(i).find('td').eq(2).text().replace(" ","T")+".csv");
    //       downloadFile(response,trs.eq(0).find('td').eq(3).text()+"_"+$("#start_date_row").val().split(" ")[0]+"T"+$("#start_date_row").val().split(" ")[1]+"0"+"_"+$("#end_date_row").val().split(" ")[0]+"T"+$("#end_date_row").val().split(" ")[1] + "0.csv");
    //       $("#on_load_wait").hide();
    //     });
    //   });
    //   let result = promise;
    // }catch(err){
    //   //console.log(err)
    // }
    // finally {
    //   $("#on_load_wait").hide();
    // }
  });

  $("#export_data").on('click',function(){
    var table = $("#table_body");
    var trs = table.find('tr');
    //console.log(trs.length);



      for (var i = 0;i < trs.length;i++){
      //console.log(trs.eq(i).find('td').eq(1).text());
      if ($("#checkedraw" + i).prop('checked')){

        try{
          let promise = new Promise((resolve, reject) => {
            var table = $("#table_body");
            var trs = table.find('tr');
            var file_name = "";
            var settings = {
              "async": true,
              "crossDomain": true,
              "url": "http://128.199.218.27:3001/api/data/export?id="+trs.eq(i).find('td').eq(3).text()+"&st="+trs.eq(i).find('td').eq(6).text()+"&ed=" +trs.eq(i).find('td').eq(7).text(),
              "method": "GET"
            }     
            file_name = trs.eq(i).find('td').eq(3).text()+"_"+trs.eq(i).find('td').eq(6).text()+"_"+trs.eq(i).find('td').eq(7).text();
            //console.log(settings);
            $.ajax(settings).done(function (response) {
              // downloadFile(response,trs.eq(i).find('td').eq(3).text()+"__"+trs.eq(i).find('td').eq(1).text().replace(" ","T")+"__"+trs.eq(i).find('td').eq(2).text().replace(" ","T")+".csv");
              downloadFile(response,file_name + ".csv");
              //console.log(file_name)
              // downloadFile(response,trs.eq(i).find('td').eq(3).text()+"_"+trs.eq(i).find('td').eq(6).text()+"_"+trs.eq(i).find('td').eq(7).text())
            });
          });
          let result = promise;
        }catch(err){
          //console.log(err)
        }
      }
    }





    
    // //console.log(tds.eq(3).text()); //device id
    // //console.log(tds.eq(1).text()); //start time
    // //console.log(tds.eq(2).text()); //stop time

    // var id = $tds.eq(1).text();
    // var status = $tds.eq(6).text();

    // device_id = device_arr[$("#SELECT_DEVICE_ID").val() - 1].id
    // //console.log(device_id)
    // http://128.199.218.27:3001/api/data/export?id=WS000001&st=2019-09-19T11:29:00.000&ed=2019-09-19T11:30:00.000

    
    // var settings = {
    //   "async": true,
    //   "crossDomain": true,
    //   "url": "http://128.199.218.27:3001/api/data/export?id="+tds.eq(3).text()+"&st="+tds.eq(1).text()+"&ed=" +tds.eq(2).text(),
    //   "method": "GET"
    // }

    // $.ajax(settings).done(function (response) {
    //   downloadFile(response,"test.csv");
    // });
  })
  $("#on_load_wait").hide();
  $("#on_load_wait_").hide();


  ininiiiiii(getCookie('device_now'));
  // progressbar(30);
  export_all_all_all();
});

// starting
// function initMap() {
//   map = new google.maps.Map(document.getElementById('map'), {
//     center: {lat: 13.8557611, lng: 100.4783308},
//     zoom: 8,
//     mapTypeId:  google.maps.MapTypeId.HYBRID
//   });
// }

function get_device() {
  //console.log('..get_device..');
  // //console.log(time_s);
  // //console.log(time_e);

  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://128.199.218.27:3000/api/device",
    // "url": "http://192.168.1.112:1880/ssmic?ch=1&id=WS20180001&st=1545619894196&ed=1545619897796", //test
    // "url": "http://127.0.0.1/Deaware/PTT_demo/GO/daq.json", //mockup
    "method": "GET"
  }

  $.ajax(settings).done(function (response) {
    //console.log(response + "OK");
    var val=response;
    data_table_json = val
    $("#index").val(val.length);
    for(var i=0;i<val.length;i++){
      /* This is config map */
      // //console.log(val[i].id)
      /*var view = '<input type="button" value="View Dashboard" onclick="window.location.href='localhost/source/view_chart.php?device=["WS000001"]'" />'*/
      var device = [val[i].id]
      var view = '<a href=\'view_chart.php?device='+JSON.stringify(device)+'&token=<?php echo $_SESSION['token']; ?>&email=<?php echo $_SESSION["email"];?>\'> <button class="btn btn-info view" id="open_dashobard"style=type="button"padding-left: 0px;padding-right: 0px;padding-left: 0px;>View Dashboard</button> </a>'
      //var val=response.reverse();
      var deferent = (Math.floor(new Date().getTime()/1000.0 - new Date(val[i].timestamp).getTime()/1000.0));
      var content_On_off = ""
      if (deferent <= 60){
        content_On_off = '<button type="button" class="btn btn-outline-success btn-sm m-0 waves-effect"><B>ONLINE</B></button>';
      }else{
        content_On_off = '<button type="button" class="btn btn-outline-danger btn-sm m-0 waves-effect"><B>OFFLINE</B></button>';
      }
      var show_data_time;
      if (!isNaN(new Date(val[i].timestamp).getHours()) || !isNaN(new Date(val[i].timestamp).getDate())){
        show_data_time = '<td id = "show_time_timestamp'+val[i].id+'" >วันที่ ' + new Date(val[0].timestamp).getDate() +" เดือน "+strMonthCut[new Date(val[0].timestamp).getMonth()]+ ' '+ (new Date(val[0].timestamp).getFullYear() + 543) + ' เวลา : ' + new Date(val[i].timestamp).getHours()+":"+new Date(val[i].timestamp).getMinutes()+":"+new Date(val[i].timestamp).getSeconds()+" น."+'</td>'
      }else{
        show_data_time = '<td id = "show_time_timestamp'+val[i].id+'" >วันที่ - เดือน - เวลา : -:-:- น.</td>'; 
      }
      $("#device_table_body").append(
        '<tr id = "map_'+val[i].id+'">'+
          '<td>'+
            '<div class="custom-control custom-checkbox">'+
              '<input type="checkbox" class="custom-control-input" id="checked'+i+'" name="checked'+i+'" value="'+val[i].id+'">'+
              '<label class="custom-control-label" for="checked'+i+'"></label>'+
            '</div>'+
          '</td>'+
          // '<td>'+(i+1)+'</td>'+
          '<td>'+val[i].id+'</td>'+
          '<td>'+val[i].info+'</td>'+
          '<td>'+val[i].lat+'</td>'+
          '<td>'+val[i].long+'</td>'+
          show_data_time+
          // '<td>'+val[i].port+'</td>'+ 
          '<td id = "content_On_off'+val[i].id+'">'+
            content_On_off+
          '</td>'+
          '<td id = "stream'+val[i].id+'">'+
            ((val[i].stream == 0) ? '<button type="button" class="btn btn-outline-danger btn-sm m-0 waves-effect"><B>INACTIVE</B></button>' : '<button type="button" class="btn btn-outline-success btn-sm m-0 waves-effect"><B>ACTIVE</B></button>')+
          '</td>'+
          // '<td>'+view+'</td>'+
          // view+
          "<td hidden>"+i+"</td>"+
        '</tr>'
      );

    }

    var table_d = $("#device_table").DataTable({
      destroy: true,
      dom: 'Bfrtip',
      lengthChange: true,
      lengthMenu: [[-1], [ "All"]],
      buttons: [
        {
          extend: 'csv',
          title: "LIST_DATA",
          text: 'EXPORT DEVICE',
          exportOptions: {
            columns: [ 0, 1, 2, 5 ],
            modifier: {
              search: 'none',
              columns: [ 0, 1, 2, 3 ]
            }
          }
        }
      ]
    });



    init_menu_config(val[0]);


    var maps = new google.maps.Map(document.getElementById('map_device'), {
      zoom: 10,
      mapTypeId:  google.maps.MapTypeId.HYBRID,
      center: {
        lat: 13.751766, lng: 100.529284
    }});

    for (i = 0; i < val.length; i++) { 
      
      var image = {				
        url:'../img/new2_mark.png',
      };

      var deferent = (Math.floor(new Date().getTime()/1000.0 - new Date(val[i].timestamp).getTime()/1000.0));
      $("#index").val(val.length);
      var content_On_off = ""
      if (deferent <= 60){
        content_On_off = '<p class="text-success"><i class="fas fa-circle text-success"></i> ONLINE</p>';
      }else{
        content_On_off = '</i><p class="text-muted"><i class="fas fa-circle text-muted"></i> OFFLINE</p>';
      }
      
      content_in_map.push('<div class="pinName" style="color:black"><b>'+content_On_off+'</b></div><hr><div class="pinName" style ="color:black"><b>Device: </b>'+val[i].id+'<b><br>Longitude:</b> '+val[i].long+'<br><b>Latitude:</b>'+val[i].lat+'</div>')
    }
    for (i = 0; i < val.length; i++) { 
      var myLatLng = {lat: parseFloat(val[i].lat), lng: parseFloat(val[i].long)};

      marker = new google.maps.Marker({
        position: new google.maps.LatLng(myLatLng.lat, myLatLng.lng),
        map: maps,
        title: "DEIVCE ID: " + val[i].id,
        icon: image,
      });

      info = new google.maps.InfoWindow();

      $("#map_"+val[i].id).click( (function(marker, i, val) {
        return function(){
          var deferent = (Math.floor(new Date().getTime()/1000.0 - new Date(val[i].timestamp).getTime()/1000.0));
          if (deferent <= 60){
            content_On_off = '<p class="text-success"><i class="fas fa-circle text-success"></i> ONLINE</p>';
          }else{
            content_On_off = '</i><p class="text-muted"><i class="fas fa-circle text-muted"></i> OFFLINE</p>';
          }
          
          // content_in_map.push('<div class="pinName" style="color:black"><b>'+content_On_off+'</b></div><hr><div class="pinName" style ="color:black"><b>Device: </b>'+val[i].id+'<b><br>Longitude:</b> '+val[i].long+'<br><b>Latitude:</b>'+val[i].lat+'</div>')
          var out = '<div class="pinName" style="color:black"><b>'+content_On_off+'</b></div><hr><div class="pinName" style ="color:black"><b>Device: </b>'+val[i].id+'<b><br>Longitude:</b> '+val[i].long+'<br><b>Latitude:</b>'+val[i].lat+'</div>';
          info.setContent(out);
          info.open(maps, marker);
        }
      }) (marker, i, val) );
      google.maps.event.addListener(marker, 'click', (function(marker, i, val) {
      return function() {
        var deferent = (Math.floor(new Date().getTime()/1000.0 - new Date(val[i].timestamp).getTime()/1000.0));
        if (deferent <= 60){
            content_On_off = '<p class="text-success"><i class="fas fa-circle text-success"></i> ONLINE</p>';
          }else{
            content_On_off = '</i><p class="text-muted"><i class="fas fa-circle text-muted"></i> OFFLINE</p>';
          }
          
          // content_in_map.push('<div class="pinName" style="color:black"><b>'+content_On_off+'</b></div><hr><div class="pinName" style ="color:black"><b>Device: </b>'+val[i].id+'<b><br>Longitude:</b> '+val[i].long+'<br><b>Latitude:</b>'+val[i].lat+'</div>')
          var out = '<div class="pinName" style="color:black"><b>'+content_On_off+'</b></div><hr><div class="pinName" style ="color:black"><b>Device: </b>'+val[i].id+'<b><br>Longitude:</b> '+val[i].long+'<br><b>Latitude:</b>'+val[i].lat+'</div>';
          info.setContent(out);
          info.open(maps, marker);
      }
      })(marker, i, val));

    }

  });
}

function input_date() {

  // select date RAW
  $('#start_date_row').datetimepicker({
    timepicker:true,
    format:'Y-m-d H:i:s'
  });
  $('#end_date_row').datetimepicker({
    timepicker:true,
    format:'Y-m-d H:i:s'
  });

  $('#end_date_row_export').datetimepicker({
    timepicker:true,
    format:'Y-m-d H:i:s'
  });


  $('#start_date_row_export').datetimepicker({
    timepicker:true,
    format:'Y-m-d H:i:s'
  });

  // select date Sise nav
  // $('#start_date').datetimepicker({
  //   timepicker:false,
  //   format:'d-m-Y'
  // });
  // $('#end_date').datetimepicker({
  //   timepicker:false,
  //   format:'d-m-Y'
  // });

}

function btn_start() {
  // page 1
  $('#GCH1').click();
  $('#GCH2').click();
  $('#GCH3').click();
  $('#map_box').click();
  $('#map_box1').click();
  $('#batt_box').click();
  $('#temp_box').click();
  $('#temper').text(30);

  // page 2
  $('#GROW').click();

  $('#search').click(function() {
    if ($("#SELECT_DEVICE_ID").val() !=  0){
      device_id = device_arr[$("#SELECT_DEVICE_ID").val() - 1].id
      // //console.log(device_id)
      var start_date__ = moment(($('#start_date_row').val()));
      var start_date__is_ok = start_date__.add(-(param_get_timezone), 'hours');


      var end_date__ = moment(($('#end_date_row').val()));
      var end_date__is_ok = end_date__.add(-(param_get_timezone), 'hours');


      //get_row(($('#start_date_row').val()),($('#end_date_row').val()),($('#SELECT_PERIOD').val()));
      console.log("============= debug ======== search device =============")
      console.log(start_date__is_ok.format("YYYY-MM-DD hh:mm:ss"))
      console.log(end_date__is_ok.format("YYYY-MM-DD hh:mm:ss"))
      console.log("========================================================")
      get_row(start_date__is_ok.format("YYYY-MM-DD hh:mm:ss"),end_date__is_ok.format("YYYY-MM-DD hh:mm:ss"),($('#SELECT_PERIOD').val()));
    }else{
      alert("Please select DEVICE_ID")
    }
  });

  $('#record_data').click(function() {
    $('#sent_data').click();
  });

}



function logout(){
	// Submit
	try {
    $("#logout").click( function (){
      window.location.replace("../index.html");
    });
	} catch (e) {
		console.log(e);
	}

}


function delete_device(){

	try {
    $("#delete_device").click( function (){
      // //console.log(this);
      var device = [];
      for (var i = 0;i < data_table_json.length;i++){
        if ($("#checked"+i).prop('checked')){
          device.push($("#checked"+i).val());
          var settings = {
            "async": true,
            "crossDomain": true,
            "url": "http://128.199.218.27:3000/api/device",
            "method": "DELETE",
            "headers": {
              "Content-Type": "application/javascript",
              "X-Requested-With": "XMLHttpRequest",
              "Authorization": "Token <?php echo $_SESSION['token'];?>",
            },
            "data": "{\"id\":\""+$("#checked"+i).val()+"\"}"
          }
          // //console.log(settings);
          $.ajax(settings).done(function (response) {
            // //console.log(response);
          });
        }
      }
      // //console.log(device)
    });
	} catch (e) {
		console.log(e);
	}

}

function modal_close(){
	try {
    $("#modal_close").click( function (){
      $("#text_device").val("") ;
    });
	} catch (e) {
		console.log(e);
	}

}

function modal_confirm(){
	try {
    $("#modal_confirm").click( function (){
      // //console.log('{"device":{"id":"'+$("#text_device").val()+'"}}');
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://128.199.218.27:3000/api/device/add",
        "method": "POST",
        "headers": {
          "Content-Type": "application/json",
          "X-Requested-With": "XMLHttpRequest",
          "Authorization": "Token <?php echo $_SESSION['token'];?>"
        },
        "processData": false,
        "data": '{"device":{"id":"'+$("#text_device").val()+'"}}'
      }
      // //console.log(settings);
      $.ajax(settings).done(function (response) {
        // //console.log(response);
        alert("INPUT NEW DEVICE SUCCESS.");
        location.reload();
      });
      $('#exampleModal').modal('hide')
      $("#text_device").val("")
    });
  } catch (e) {
    console.log(e);
  }
}


function record_data(){
	try {
    $("#record_data").click( function (){
      $('#record_data').attr("disabled", true);
      serin = setInterval(function(){   get_realtime(); }, 1000);
    });
	} catch (e) {
		console.log(e);
	}
}

function stop_data(){
	try {
    $("#stop_data").click( function (){
      $('#record_data').prop('disabled', false);
      clearInterval(serin);
    });
	} catch (e) {
		console.log(e);
	}
}
function init_menu_config(val){
  //var val=response.reverse();
  var deferent = (Math.floor(new Date().getTime()/1000.0 - new Date(val.timestamp).getTime()/1000.0));
  $('#side_id').text(val.id);
  if (deferent <= 60){
    $('#side_status').text("ONLINE");
  }else{
    $('#side_status').text("OFFLINE");
  }
}
function open_dashboard(){
  $('#open_dashobard').click(function() {
    var device = [];
    for (var i = 0;i < data_table_json.length;i++){
      if ($("#checked"+i).prop('checked')){
        device.push($("#checked"+i).val());
        //console.log(device)
      }
    }
    if (device.length > 0){
      window.open('./view_chart.php?device='+JSON.stringify(device) + "&token=<?php echo $_SESSION['token']; ?>&email=<?php echo $_SESSION["email"];?>", '_blank');
    }else{
      alert("Select Device");
    }
  });
}
function check_status(){
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://128.199.218.27:3000/api/device",
    "method": "GET"
  }

  $.ajax(settings).done(function (response) {
    var val=response;
    // //console.log(val);
    // show_time_timestamp content_On_off stream
    for (var i = 0;i < val.length;i++){
      var deferent = (Math.floor(new Date().getTime()/1000.0 - new Date(val[i].timestamp).getTime()/1000.0));
      var content_On_off = ""
      if (deferent <= 60){
        content_On_off = '<button type="button" class="btn btn-outline-success btn-sm m-0 waves-effect"><B>ONLINE</B></button>';
      }else{
        content_On_off = '<button type="button" class="btn btn-outline-danger btn-sm m-0 waves-effect"><B>OFFLINE</B></button>';
      }
    $("#content_On_off"+val[i].id).html("");
    $('#content_On_off'+val[i].id).append(content_On_off);
      var show_data_time;
      if (!isNaN(new Date(val[i].timestamp).getHours()) || !isNaN(new Date(val[i].timestamp).getDate())){
        show_data_time = 'วันที่ ' + new Date(val[0].timestamp).getDate() +" เดือน "+strMonthCut[new Date(val[0].timestamp).getMonth()]+ ' '+ (new Date(val[0].timestamp).getFullYear() + 543) + ' เวลา : ' + new Date(val[i].timestamp).getHours()+":"+new Date(val[i].timestamp).getMinutes()+":"+new Date(val[i].timestamp).getSeconds()+" น."
      }else{
        show_data_time = 'วันที่ - เดือน - เวลา : -:-:- น.'; 
      }
      $("#show_time_timestamp"+val[i].id).text(show_data_time);
      var content_out_stream = ((val[i].stream == 0) ? '<button type="button" class="btn btn-outline-danger btn-sm m-0 waves-effect"><B>INACTIVE</B></button>' : '<button type="button" class="btn btn-outline-success btn-sm m-0 waves-effect"><B>ACTIVE</B></button>')
      $("#stream"+val[i].id).html("");
      $('#stream'+val[i].id).append(content_out_stream);  
    }

  });
}




function set_select_option(){
  var content_out_stream = "";
  content_out_stream += "<option selected value = '0' selected>SELECT DEIVCE ID</option>"
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://128.199.218.27:3000/api/device",
    "method": "GET"
  }

  $.ajax(settings).done(function (response) {
    var val=response;
    //console.log(val);
    device_arr = val;
    for (var i = 0;i< val.length;i++){
      content_out_stream += "<option value = '"+(i+1)+"'>"+val[i].id+"</option>"
    }
    // content_out_stream += "<option value = '' selected>SELECT DEIVCE ID</option>"
    $("#SELECT_DEVICE_ID").html("");
    $('#SELECT_DEVICE_ID').append(content_out_stream);  

  });
}

function link_fc_read_time_page(){
  console.log("link_fc_read_time_page")
  try{
    var device = getCookie("device_now");
    var device_obj = JSON.parse(device)
    var check = false;
    for (var i = 0;i < device_obj.length;i++){
      if (device_obj[i] != null ){
        window.location.replace("../source/view_chart.php?device=[\"" + device_obj[i] + "\"]&timezone=<?php echo $_GET['timezone']; ?>");
        check = true;
        break;
      }
    }
    if (!check){
      alert("Please select device");
    }
  } catch (err) {

  }
}


function downloadFile(data, fileName, type="text/plain") {
  // Create an invisible A element
  const a = document.createElement("a");
  a.style.display = "none";
  document.body.appendChild(a);

  // Set the HREF to a Blob representation of the data to be downloaded
  a.href = window.URL.createObjectURL(
    new Blob([data], { type })
  );

  // Use download attribute to set set desired file name
  a.setAttribute("download", fileName);

  // Trigger the download by simulating click
  a.click();

  // Cleanup
  window.URL.revokeObjectURL(a.href);
  document.body.removeChild(a);
}


function ininiiiiii(cook){
  $("#select_device_show").html("");
  var dev_obj = JSON.parse(cook)
  for (var i = 0;i < dev_obj.length;i++){
    if (dev_obj[i] != null){
      //console.log("#checked" + i)
      $("#checked" + i ).prop('checked',true)
      $("#select_device_show").append(
        "<tr>"+
          "<td>"+ dev_obj[i] + " " +
          "<i id = 'on_load_wait____new_1"+dev_obj[i]+"' class='fa fa-spinner fa-spin' style = 'font-size: 20px;'></i><span id = 'on_load_wait____new_span_1"+dev_obj[i]+"'> Wait.</span>"+
          "<i id = 'on_load_wait____new_2"+dev_obj[i]+"' class='fas fa-check-circle' style= 'color:green;'></i><span id = 'on_load_wait____new_span_2"+dev_obj[i]+"'> Succeed. <a target = '_blank'  style = 'text-decoration: underline;color: blue;' id = 'link_"+dev_obj[i]+"' href> Download</a></span>"+
          "<i id = 'on_load_wait____new_3"+dev_obj[i]+"' class='fas fa-exclamation-circle' style= 'color:#d1b805;'></i><span id = 'on_load_wait____new_span_3"+dev_obj[i]+"'> Not Found Data.</span></td>"+
          // <i id = 'on_load_wait____new_1' class='fas fa-check-circle'></i>
          // <i id = 'on_load_wait____new_2' class='fa fa-spinner fa-spin' style = 'font-size: 20px;'></i>
          // <i id = 'on_load_wait____new_3' class="fas fa-exclamation-circle"></i>
        "</tr>"
      );
    }
    $("#on_load_wait____new_1"+dev_obj[i]).hide();
    $("#on_load_wait____new_2"+dev_obj[i]).hide();
    $("#on_load_wait____new_3"+dev_obj[i]).hide();
    
    $("#on_load_wait____new_span_1"+dev_obj[i]).hide();
    $("#on_load_wait____new_span_2"+dev_obj[i]).hide();
    $("#on_load_wait____new_span_3"+dev_obj[i]).hide();

  }
}
function setCookie(cname,cvalue,exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires=" + d.toGMTString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "[]";
}

function checkCookie() {
  var user=getCookie("username");
  if (user != "") {
    alert("Welcome again " + user);
  } else {
     user = prompt("Please enter your name:","");
     if (user != "" && user != null) {
      //  setCookie("username", user, 30);
     }
  }
}

function progressbar(width){
  var i = 0;
  if (i == 0) {
    i = 1;
    var elem = document.getElementById("myBar");
    // var width = 1;
    // var id = setInterval(frame, 10);
    elem.style.width = width + "%";
    // function frame() {
    //   if (width >= 100) {
    //     clearInterval(id);
    //     i = 0;
    //   } else {
    //     width++;
    //     elem.style.width = width + "%";
    if (width == 100){
      $("#persent").text("Complete : "+width+"%")
    }else{
      $("#persent").text("Please wait preparing file .csv : "+width.toFixed(2)+"%")
    }
    //   }
    // }
  }
}


function export_all_all_all(){
  $("#export_all_all_all").click(function (){
    var r = confirm("You want Export file.");
    // for v1
    var end_date = $("#end_date_row").val().split(" ")[0]+"T"+$("#end_date_row").val().split(" ")[1] + "";
    var start_date = $("#start_date_row").val().split(" ")[0]+"T"+$("#start_date_row").val().split(" ")[1]+"";
    if (param_get_timezone == 0){
      end_date = moment(end_date).format('YYYY-MM-DDTHH:mm:ss'); 
      start_date = moment(start_date).format('YYYY-MM-DDTHH:mm:ss');  
    }else if (param_get_timezone == 7){
      end_date = moment(end_date).add(-7,"hours").format('YYYY-MM-DDTHH:mm:ss'); 
      start_date = moment(start_date).add(-7,"hours").format('YYYY-MM-DDTHH:mm:ss');  
    }
    // for v2
    // var end_date = $("#end_date_row_export").val().split(" ")[0]+"T"+$("#end_date_row_export").val().split(" ")[1] + "";
    // var start_date = $("#start_date_row_export").val().split(" ")[0]+"T"+$("#start_date_row_export").val().split(" ")[1]+"";

    if (r == true){
      var arr_device = getCookie('device_now');
      var new_arr = Array();
      var ttt = JSON.parse(arr_device);
      console.log("================================");
      console.log("==== debug cookie old ==========")
      console.log("================================");
      console.log("start :: "+ start_date)
      console.log("end   :: " + end_date)
      console.log("================================");
      var i = 0;
      var o = 0;
      while (i < ttt.length){
        if (ttt[i] != null){
          new_arr[o] = ttt[i]
          o++;
        }
        i++;
      }
      arr_device = JSON.stringify(new_arr);
      console.log("================================");
      console.log("==== debug cookie new ==========")
      console.log("================================");
      console.log(arr_device)
      console.log("================================");
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://ssmonitors.site:3002/api/export",
        "method": "POST",
        "headers": {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "http://ssmonitors.site"
        },
        "processData": false,
        "data": "{\n\t\"id\":"+arr_device+",\n\t\"st\":\""+start_date+"\",\n\t\"ed\":\""+end_date+"\"\n}"
      }
      // //console.log(settings);
      $.ajax(settings).done(function (response) {
        //console.log(response);
        // clear_interval();
        setCookie("REQ", response.REQ, 99999);
        get_export_req();
        // $("#export_all_all_all").hide();
        $("#export_all_all_all").attr("disabled", "disabled"); 
        $("#export_all_all_all_zip").attr("disabled", "disabled"); 
        interval_variable_global = setInterval(get_export_req, 1000);
      });
    }
  });
}

function get_export_req(req){
  var settings = {
    "url": "http://128.199.218.27:3002/api/" + getCookie("REQ"),
    "method": "GET",
  }
  //console.log(settings);
  $.ajax(settings).done(function (response) {
    console.log(response);
    var total = 0;
    for (var i = 0;i < response.PG.length;i++){
      $("#on_load_wait____new_1"+response.ID[i]).hide();
      $("#on_load_wait____new_2"+response.ID[i]).hide();
      $("#on_load_wait____new_3"+response.ID[i]).hide();
      $("#on_load_wait____new_span_1"+response.ID[i]).hide();
      $("#on_load_wait____new_span_2"+response.ID[i]).hide();
      $("#on_load_wait____new_span_3"+response.ID[i]).hide();

    }
    for (var i = 0;i < response.PG.length;i++){
        $("#on_load_wait____new_1"+response.ID[i]).show();
        $("#on_load_wait____new_span_1"+response.ID[i]).show();

        // const a = document.createElement('link_'+response.ID[i]);
        $('#link_'+response.ID[i]).attr("href", "http://ssmonitors.site/export/"+response.FNAME[i]);


        
    }
    for (var i = 0;i < response.PG.length;i++){
      if (response.PG[i] == -1){
        $("#on_load_wait____new_1"+response.ID[i]).hide();
        $("#on_load_wait____new_3"+response.ID[i]).show();

        $("#on_load_wait____new_span_1"+response.ID[i]).hide();
        $("#on_load_wait____new_span_3"+response.ID[i]).show();
      }
      if (response.PG[i] == 100){
        $("#on_load_wait____new_1"+response.ID[i]).hide();
        $("#on_load_wait____new_2"+response.ID[i]).show();
        $("#on_load_wait____new_3"+response.ID[i]).hide();

        $("#on_load_wait____new_span_1"+response.ID[i]).hide();
        $("#on_load_wait____new_span_2"+response.ID[i]).show();
        $("#on_load_wait____new_span_3"+response.ID[i]).hide();
      }
      if (response.PG[i] != 0){
        total += 100;
      }
    }
    //console.log((total / response.PG.length));
    progressbar((total / response.PG.length));
    if ((total / response.PG.length) == 100){
      //console.log("OK")
      clear_interval(response.FNAME);
      // for (var i = 0;i < response.FNAME.length;i++){
      //   if (response.PG[i] != -1){
      //     //console.log("load")
      //     window.open("http://ssmonitors.site/export/"+response.FNAME[i])
      //   }
      // }
    }
  });
}

function clear_interval(FNAME){
  clearInterval(interval_variable_global)
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://128.199.218.27:3002/api/zip",
    "method": "POST",
    "headers": {
      "Content-Type": "application/json",
      "Accept": "*/*",
      "Cache-Control": "no-cache",
      "cache-control": "no-cache"
    },
    "processData": false,
    "data": "{\n\t\"fname\":"+JSON.stringify(FNAME)+"}"
  }
  console.log(settings)
  $.ajax(settings).done(function (response) {
    console.log(response.ZIPNAME);
    $("#export_all_all_all").show();
    $("#export_all_all_all").removeAttr("disabled"); 
    $("#export_all_all_all_zip").removeAttr("disabled"); 
    $("#export_all_all_all_zip").click(function (){
      window.open("http://ssmonitors.site/export/"+response.ZIPNAME)
    });
  });

}
function get_timezone(){
  // 1 = UTC
  // 2 = GMT
  if ($("#select_timezone").val() != "0"){
    console.log($("#select_timezone").val());
    switch($("#select_timezone").val()) {
      case "1":
        windown_get_param("0")
        break;
      case "2":
        windown_get_param("7")
        break;
      default:
        console.log("not OK not")
    }
  }else{
    alert("Please select Timezone")
  }
}
function windown_get_param(value){
  var url = new URL(window.location.href);
  url.searchParams.set('timezone',value);
  window.location.href = url.href;
}
</script>
</body>
</html>