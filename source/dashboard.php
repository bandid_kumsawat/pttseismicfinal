<?php
  session_start();
  if(isset($_SESSION['token'])) {
      $token = $_SESSION['token'];
  } else {
    header("Location: ../index.html");
    exit;
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <link rel="icon" href="../img/logo/logo-pttep.png" type="image/png" sizes="17x17">
  <title>WIRELESS LAND SEISMIC MONITORING</title>
  <!-- Font Awesome -->
  <link href="../font/fontawesome/css/all.min.css" rel="stylesheet">
  <!-- Bootstrap core CSS -->
  <link href="../css/bootstrap.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="../css/mdb.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="../css/style.min.css" rel="stylesheet">
  <!-- Your custom datetimepicker (optional) -->
  <link href="../css/jquery.datetimepicker.min.css" rel="stylesheet">
  <!-- Your custom dataTable (optional) -->
  <link href="../css/datatables.css" rel="stylesheet">
  <style>
    .custom-checkbox{
      padding-bottom:20px;
    }
    .font-size-18{
      font-size: 16px;
      padding-top: 10px;
    }
    .scroll-bar-table{
      height: 500px;
      overflow: scroll;
    }
    .map-container{
      overflow:hidden;
      padding-bottom:56.25%;
      position:relative;
      height:0;
    }
    .map-container iframe{
      left:0;
      top:0;
      height:100%;
      width:100%;
      position:absolute;
    }

    .tr_color {color: #F5F5F5;}

    .mouse-hover{
      cursor: pointer;
    }

    .bg-device-color{
      background-color: #ffffff;
      color:#000000;
    }
    .buttons-csv{
      width: 120px;
      position:absolute !important;
      top:6px;

    }
  /* 
  p.ex1 {display: none;}
  p.ex2 {display: inline;}
  p.ex3 {display: block;}
  p.ex4 {display: inline-block;} 
  */
    #show_side_hide{
      /* display: none; */
      display: block;
      padding: 0px 30px 0px 30px;
      width:40%;
    }
    #show_image_seimic{
      display: inline-block;
      padding: 0px 30px 0px 30px;
      width:40%;
    }
    @media (max-width: 1199.98px) {
      #show_side_hide {
        padding: 0px 30px 0px 30px;
        display: block;
      } 
    }
    #content_1{
      padding-left:25px;
    }

    /* style online and offline */
    .text-online {
      color: #28a745 !important;
      font-size: 0.9rem;
      font-weight: bold;
    }

    .text-offline {
      color: #dc3545 !important;
      font-size: 0.9rem;
      font-weight: bold;
    }

    .text-active {
      color: #28a745 !important;
      font-size: 0.9rem;
      font-weight: bold;
    }

    .text-inactive {
      color: #dc3545 !important;
      font-size: 0.9rem;
      font-weight: bold;
    }

    /* new nav active */
    .nav-active{
      border-bottom:6px solid #000000;
      border-radius: 3px;
    }
    .nav-pills .nav-link.active,
    .nav-pills .show > .nav-link {
      border-bottom: 5px solid #007bff;
    }
    .nav-menu {
      color: #007bff;
    }

    .dropdown-menu {
      left: auto;
      right: 0;
      float: right;
    }
    .nav-pills .nav-link {
      border-radius: 0rem;
    }
    .bg-image {
      background-image: url(../login/images/BG_09.jpg);
    }
    .image-image-image{
      /* background-image: url(../login/images/522343-earthquake.jpg); */
    }
  </style>
</head>

<body class=" lighten-3">

  <!--Main Navigation-->
  <header>

    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
      <div class="container-fluid">

        <!-- Collapse -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Links -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

          <!-- Left -->
          <ul class="nav nav-pills nav-pills-primary mr-auto" role="tablist">
            <li class="nav-item" style = "padding-right: 12px;">
              <a class="nav-link active" style = "padding-bottom: 0px !important;padding-right: 4px !important;padding-left:4px !important;" data-toggle="tab" href="#link1" role="tablist" aria-expanded="true">
                <i class="fas fa-tram mr-1"></i> <B>DEVICE MANAGER</B>
              </a>
            </li>
            <!-- <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#link1" role="tablist" aria-expanded="false">
                <i class="fas fa-chart-bar mr-1"></i> <B>DASHBOARD</B>
              </a>
            </li> -->

            <li class="nav-item " style = "padding-right: 12px;">
              <a class="nav-link" style = "padding-bottom: 0px !important;padding-right: 4px !important;padding-left:4px !important;" href='#' onclick = "link_fc_read_time_page();" role="tablist" aria-expanded="false">
              <!-- ?token=<?php echo $_SESSION['token'];?>&email=<?php echo $_SESSION["email"]; ?> ./view_chart.php?device=["WS000001"]          --> 
                <i class="fas fa-chart-line mr-1"></i> <B>REAL TIME</B>
              </a>
            </li>
            

            <li class="nav-item" style = "padding-right: 12px;">
              <a class="nav-link " style = "padding-bottom: 0px !important;padding-right: 4px !important;padding-left:4px !important;" href="rawdata.php?timezone=<?php echo $_GET['timezone'];?>" role="tablist" aria-expanded="false">
                <i class="fas fa-table mr-1"></i><B>DAQ DATA</B>
              </a>
            </li>


            <!--<li class="nav-item">
              <button  style = "width: 100%;" type="button" class="btn btn-info btn-sm" id="open_dashobard">View Dashboard</button>
            </li>-->


          </ul>

          <ul class="nav nav-menu">
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown">
                <i class="fas fa-address-card mr-1"></i><B><?php echo $_SESSION['email'] ?></B>
              </a>
              <div class="dropdown-menu">
                <center>
                 <B><?php echo $_SESSION['email'] ?></B>
                  <img src = "../login/images/admin.png" width="100px;"/>
                  <a class="dropdown-item">
                    <button id = "c_time"class='btn btn-outline-info btn-sm' data-toggle="modal" data-target="#change_datetime_show">Change Time</button><br>
                    <button id = "logout"class='btn btn-outline-danger btn-sm'>Logout</button>
                </a>
              </center>
              </div>
            </li>
          </ul>

          
        </div>

      </div>
    </nav>
    <!-- Navbar -->

    <!-- Sidebar -->
    <div class="sidebar-fixed position-fixed bg-image">

      <!-- <a class="logo-wrapper waves-effect"> -->
      <div class="mb-4">
        <center><img src="../img/logo/logo-pttep.png"  alt="" style="width: 70%; height: 70%"></center>
      </div>
      <!-- </a> -->

      <!-- <M>Menu Config</M> -->
      <div class="mb-2"></div>

      <table class="table table-hover">
        <tbody>
          <!-- <tr>
            <th>Device ID</th>
            <td id="side_id"></td>
          </tr> -->
          <tr>
            <th>Project</th>
            <td>WIRELESS LAND SEISMIC</td>
          </tr>
          <tr>
            <th>Date</th>
            <td id="real_date"></td>
          </tr>
    </tbody>
  </table>

  <div class='row'>
    <div class='col-sm-12'>
      <!-- <button  style = "width: 100%;" type="button" class="btn btn-info btn-sm" id="open_dashobard">View Dashboard</button> -->
    </div>
  </div>
</div>
<!-- Sidebar -->

</header>
<!--Main Navigation-->

<!--Main layout-->
<main class="pt-5 mx-lg-5 image-image-image">
  <div class="container-fluid mt-5">
    <div class="tab-content tab-space">
    <!-- Page 2 -->
    <div class="tab-pane" id="link2" aria-expanded="true">
      <!-- Graph RAW -->
      <div class="col-md-12 mb-4 fadeIn">
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8  fadeIn">
            <div class="card ">
              <div class="container">
                <div class="col-12">
                  <div class="row">
                    <div class="col-5">
                      <p class="mt-2">START DATE</p>
                      <div class="input-group mt-3 mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fas fa-calendar-week"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Start Date" aria-label="Start_Date" id="start_date_row" value="">
                      </div>
                    </div>
                    <div class="col-5">
                      <p class="mt-2">END DATE</p>
                      <div class="input-group mt-3 mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fas fa-calendar-week"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="End Date" aria-label="End_Date" id="end_date_row" value="">
                      </div>
                    </div>
                    <div class="col-2">
                      <div class="mt-4 mb-3">
                        <button type="button" class="btn blue-gradient btn-rounded" id="search">search</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-12 mb-4 fadeIn">
        <div class="card">
          <div href="#row_coll" class="card-header waves-effect" data-toggle="collapse" id="GROW">
          <i class="fas fa-chart-line fa-2x mr-2"></i><B>Graph</B></div>
          <div class="card-body collapse" id="row_coll">
            <div id="ch3" style="height: 400px; min-width: 310px"></div>
          </div>
        </div>
      </div>

      <h2>RAW Table</h2>
      <hr>
      <div class="head_table table-responsive">
        <table class="table table-hover table-bordered" id="row_table">
          <thead>
            <tr class="bg-primary tr_color">
              <th>ID Device</th>
              <th>Channel</th>
              <th>UTC</th>
              <th>TIME</th>
              <th>Payload</th>
            </tr>
          </thead>
          <tbody id="table_body">

          </tbody>
        </table>
      </div>

      <center><div class="loader" id="row_load2"></div></center>

    </div>


    <!-- page 3 -->

    <div class="tab-pane active" id="link1" aria-expanded="true">
      <div class="col-md-12 mb-4 fadeIn" style = "padding:0px !important;">
            <div class="card ">
              <div class="container">
                <div class="col-12">
                  <div class="row">
                    <div class='col-12'>
                      <center>
                        <p><div style = "color: black;font-size: 1.5em;font-weight: 700;">
                        <?php 
                          if ($_GET['timezone'] == "0"){
                            echo "Time Zone [ UTC ]";
                          }else if ($_GET['timezone'] == '7') {
                            echo "Time Zone [ GMT Thailand ]";
                          }
                        ?>
                      </div></p>
                    </center>
                    </div>
                  </div>
                </div>
              </div>
            </div>

      <div class="card">

        
        <div href="#CH1" class="card-header waves-effect" data-toggle="collapse" id="GCH1">
        <i class="fas fa-table fa-2x mr-2"></i><B>Selected Devices</B></div>
          <div class="card-body collapse" id="CH1">
            <div id="ch1" style="/*height: 400px;*/ min-width: 310px">
                <input type="checkbox" name="index" value="" id="index" checked style="display:none;">
                <div class="col-md-12 mb-4 fadeIn">
                  <div class="container-fluid ">
                    <br>
                    <h1>
                    </h1>
                    <hr>
                      <div class='row' id = ''>
                        <div class='col-sm-6'>
                          <div class='col-sm-12 scroll-bar-table'>
                            <table class="table table-hover table-bordered">
                              <thead>
                                <tr class="bg-device-color" align="center">
                                  <th scope="col" style = "font-size: 1rem;font-weight:900;"width="100%">Selected Device ID</th>
                                </tr>
                              </thead>
                              <tbody align="center" style = "font-size: 1rem !important;font-weight:500 !important;" id = "select_device_show">

                              </tbody>
                            </table>
                          </div>
                          <button  class="btn btn-success btn-font" id="record_data">Record</button>
                          <button  class="btn btn-delete btn-font waves-effect waves-light" id="stop_data">Stop</button>
                          <button  class="btn btn-delete btn-font waves-effect waves-light" id="select_all">Select All</button>
                          <button  class="btn btn-delete btn-font waves-effect waves-light" id="unselect_all">Unselect All</button>

                          <button  class="btn btn-delete btn-font waves-effect waves-light" id="add_event" data-toggle="modal" data-target="#add_event_modal">ADD EVENT</button>


                      <div class="modal fade" id="add_event_modal" tabindex="-1" role="dialog" aria-labelledby="add_eventexampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="add_eventexampleModalLabel">ADD EVENT</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                              <div class="modal-body">
                                <br>

                                <div class='row'>
                                  <div class = 'col-12'>
                                    SELECT EVENT:
                                    <select id="SELECT_EVENT_ID" class="browser-default custom-select">
                                      <option selected="" value="0">SELECT EVENT</option>
                                      <option value="1">WS000001</option>
                                      <option value="5">WS000005</option>
                                    </select>
                                  </div>
                                </div>
                              </div>
                                <div class="modal-footer">
                                  <button id = "modal_confirm_add_device_to_event" type="button" class="button-save">Save</button>
                                  <button id = "modal_close" type="button" class="button-close " data-dismiss="modal">Close</button>
                                </div>
                          </div>
                        </div>
                      </div>





                        </div>
                        <div class='col-sm-6' align='center'>
                          <div class="" id="map1_device">
                            <div id="map_device" class="z-depth-1-half" style="height: 440px"></div>
                          </div>
                        </div>
                      </div>
                      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Add Device</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                              <div class="modal-body">
                                <br>

                                <div class='row'>
                                  <div class = 'col-6'>
                                    Description Device:
                                  </div>
                                  <div class = 'col-6'>
                                    <input type='text' id = 'description_device'>
                                  </div>
                                </div>
                              </div>
                                <div class="modal-footer">
                                  <button id = "modal_confirm" type="button" class="button-save">Save</button>
                                  <button id = "modal_close" type="button" class="button-close " data-dismiss="modal">Close</button>
                                </div>
                          </div>
                        </div>
                      </div>
                                           


                      <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Edit Device</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                              <div class="modal-body">
                                <br>

                                <div class='row'>
                                  <div class = 'col-6'>
                                    Description Device:
                                  </div>
                                  <div class = 'col-6'>
                                    <input type='text' id = 'description_device_edit'>
                                    <input hidden type = 'text' value = '' id = 'device_id_for_edit'/>
                                  </div>
                                </div>
                              </div>
                                <div class="modal-footer">
                                  <button id = "modal_confirm_edit" type="button" class="button-save">Save</button>
                                  <button id = "modal_close_edit" type="button" class="button-close " data-dismiss="modal">Close</button>
                                </div>
                          </div>
                        </div>
                      </div>
                      
                  </div>
                </div>
            </div>

          </div>
        </div>
        

      </div>
      <div class='row'>
        <div class='col-sm-12'>
          DEVICE TABLE<hr>
        </div>
      </div>
      <div class="table-responsive">
        <!-- <table class="table table-hover table-bordered" id="device_table">
        </table> -->
        <div class='row' style = "padding-top: 20px;">
          <div class='col-sm-6' id = "content_1" align="left">
          </div>
          <div class='col-sm-6' id = "content_2" align="right">
            <button id="add_device" class="btn btn-primary btn-font waves-effect waves-light" style = "" data-toggle="modal" data-target="#exampleModal">ADD</button>
            <button id="delete_device" class="btn btn-delete btn-font waves-effect waves-light" style = "">DELETE</button>
          </div>
        </div>
        <table class="table table-hover table-bordered" id="device_table">
                    <thead align="center">
                      <tr class="bg-primary tr_color">

                      <!-- scope="col" width="5%" -->

                        <th scope="col" width="5%">#</th>
                        <th scope="col" width="10%">Device ID</th>
                        <th scope="col" width="10%">Description</th>
                        <th scope="col" width="10%">Latitude</th>
                        <th scope="col" width="10%">Longitude</th>
                        <th scope="col" width="15%">TimeStamp</th>
                        <th scope="col" width="15%">Status</th>
                        <th scope="col" width="15%">Recorder</th>
                        <th hidden="hidden"></th>
                        <th scope="col" width="10%">Battery Level</th>
                        <th scope="col" width="10%">GPS COUNT</th>
                        <th scope='col' width='10%'>Manage</th>
                      </tr>
                    </thead>
                    <tbody id="device_table_body" align="center">
                    </tbody>
                  </table>
      </div>  
 


      <br><br><br>
      <div class='row'>
        <div class='col-sm-12'>
          EVENT TABLE<hr>
        </div>
      </div>
      <div class='row' style = "padding: 20px;padding-left: 0px;">
        <div class='col-sm-6' id = "content_1" align="left">
        </div>
        <div class='col-sm-6' id = "content_2" align="right">
          <button id="add_device_event" class="btn btn-primary btn-font waves-effect waves-light" style = "" data-toggle="modal" data-target="#exampleModal_event_add">ADD</button>
          <button id="delete_device_event" class="btn btn-delete btn-font waves-effect waves-light" style = "">DELETE</button>
        </div>
      </div>
      <div class="head_table table-responsive">
        <table class="table table-hover table-bordered" id="row_table_event">
          <thead>
            <tr class="bg-primary tr_color">
              <th><center>#</center></th>
              <th><center>ID</center></th>
              <th><center>START</center></th>
              <th><center>STOP</center></th>
              <th><center>DEVICE</center></th>
            </tr>
          </thead>
          <tbody id="table_body_event">

          </tbody>
        </table>
      </div>



    </div>

  </div>
</main>
<!--Main layout-->

<!-- add event -->
<div class="modal fade" id="exampleModal_event_add" tabindex="-1" role="dialog" aria-labelledby="exampleModal_event_add_Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModal_event_add_Label">Add Event</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
      </div>
        <div class="modal-body">
          <br>

          <div class='row'>
            <div class = 'col-12'>
              START TIME : <input type="text" class="form-control" placeholder="Start Date" aria-label="Start_Date" id="start_date_event_time" value="">
              <br>
              STOP TIME  :  <input type="text" class="form-control" placeholder="Stop Date" aria-label="Stop_Date" id="stop_date_event_time" value="">
            </div>
          </div>
        </div>
          <div class="modal-footer">
            <button id = "modal_confirm_event" type="button" class="button-save">Save</button>
            <button id = "modal_close" type="button" class="button-close " data-dismiss="modal">Close</button>
          </div>
    </div>
  </div>
</div>



<!--Footer-->
<footer class="page-footer text-center font-small primary-color-dark darken-2 mt-4" style = "visibility: visible !important;background-color: #cbefff !important;">

  <!--Copyright-->
  <div class="footer-copyright py-2">
    © 2019 Copyright:
    <a href="http://www.pttep.com/en/index.aspx" target="_blank"> pttep.com </a>
  </div>
  <!--/.Copyright-->

</footer>


<!-- change time zone -->
<div class="modal fade" id="change_datetime_show" tabindex="-1" role="dialog" aria-labelledby="change_datetime_show_ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="change_datetime_show_ModalLabel">Change Time Zone</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <br>
                <div class='row'>
                    <div class = 'col-12'>
                        SELECT TIME ZONE:
                        <select id="select_timezone" class="browser-default custom-select">
                            <option selected="" value="0">SELECT TIME ZONE</option>
                            <option value="1">UTC</option>
                            <option value="2">GMT (Thailand)</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button onclick = "get_timezone()"id = "modal_confirm_add_device_to_event" type="button" class="button-save">Save</button>
                <button id = "modal_close" type="button" class="button-close " data-dismiss="modal">Close</button>
            </div>
      </div>
    </div>
</div>



<!--/.Footer-->

<!-- SCRIPTS -->

<!-- JQuery -->
<script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="../js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.js"></script>
<!-- fontawesome -->
<script type="text/javascript" src="../font/fontawesome/js/all.min.js"></script>
<!-- datetimepicker -->
<script type="text/javascript" src="../js/jquery.datetimepicker.full.min.js"></script>
<!-- dataTables -->
<script type="text/javascript" src="../js/datatables.js"></script>
<!-- Moment -->
<script type="text/javascript" src="../js/moment.js"></script>
<!-- Google Maps -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDZua0o5Tg5lNTmzsUdgAQkdMYk7yvGIVo&callback=initMap" async defer></script>
<!-- Highstock -->
<script type="text/javascript" src="../js/addons/Highstock.js"></script>
<script type="text/javascript" src="../js/addons/boost.js"></script>
<script type="text/javascript" src="../js/modules/exporting.js"></script>
<script type="text/javascript" src="../js/modules/export-data.js"></script>

<!-- JS Function -->
<script type="text/javascript" src="../js/function/page1.js"></script>
<script type="text/javascript" src="../js/function/page2.js"></script>
<script type="text/javascript" src="../js/function/page3.js"></script>
<script type="text/javascript" src="../js/function/option.js"></script>

<!-- Initializations -->
<script type="text/javascript">
var strMonthCut = ["ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."];
const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
var graph_arr=[];
var map,table_s,marker;
var device_status,device_id="WS000001";
var data_table_json
var maps,marker, i, info,content_in_map = [],image,position_count;
window.position_count = 0;
var param_get_timezone = <?php echo $_GET['timezone'];?>

$( document ).ready(function() {
  modal_confirm_edit();
  open_dashboard();
  modal_close();
  modal_confirm();
  record_data();
  stop_data();
  //init_menu_config();
  setInterval(function(){ check_status();  }, 2*1000);
  delete_device();
  logout();
  console.log("ready!");
  select_all_all();
  unselect_all_all();

  // starting-----------------------------------------

  // $('#start_date_row').val(moment().format('YYYY-MM-DD HH:mm:ss'));
  // $('#end_date_row').val(moment().format('YYYY-MM-DD HH:mm:ss'));
  // $('#start_date_row').val(moment().format('2019-02-05 12:11:53')); //test
  // $('#end_date_row').val(moment().format('2019-02-05 12:11:54')); //test

  try {
    // initMap();
    input_date();
    btn_start();
    new WOW().init();
  } catch (e) {
    location.reload();
  }

  // page 1-------------------------------------------
  var serin;
  batt(0);

  // page 2-------------------------------------------
  //get_row(time_set($('#start_date_row').val()),time_set($('#end_date_row').val()));

  // page 3-------------------------------------------

  // get_device();

  // option-------------------------------------------
  real_date();

  // test---------------------------------------------
  console.log(	new Date().getTime() );
  get_device();


});

// starting
// function initMap() {
//   map = new google.maps.Map(document.getElementById('map'), {
//     center: {lat: 13.8557611, lng: 100.4783308},
//     zoom: 8,
//     mapTypeId:  google.maps.MapTypeId.HYBRID
//   });
// }

function get_device() {
  console.log('..get_device..');
  // console.log(time_s);
  // console.log(time_e);

  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://128.199.218.27:3000/api/device",
    // "url": "http://192.168.1.112:1880/ssmic?ch=1&id=WS20180001&st=1545619894196&ed=1545619897796", //test
    // "url": "http://127.0.0.1/Deaware/PTT_demo/GO/daq.json", //mockup
    "method": "GET"
  }

  $.ajax(settings).done(function (response) {
    console.log(response + "OK");
    var val=response;
    data_table_json = val
    $("#index").val(val.length);
    for(var i=0;i<val.length;i++){
      /* This is config map */
      // console.log(val[i].id)
      /*var view = '<input type="button" value="View Dashboard" onclick="window.location.href='localhost/source/view_chart.php?device=["WS000001"]'" />'*/
      var device = [val[i].id]
      var view = '<a href=\'view_chart.php?device='+JSON.stringify(device)+'&token=<?php echo $_SESSION['token']; ?>&email=<?php echo $_SESSION["email"];?>\'> <button class="btn btn-info view" id="open_dashobard"style=type="button"padding-left: 0px;padding-right: 0px;padding-left: 0px;>View Dashboard</button> </a>'
      //var val=response.reverse();

      var deferent = (Math.floor(new Date().getTime()/1000.0 - new Date(val[i].timestamp).getTime()/1000.0));
      
      var content_On_off = ""
      if (deferent <= 60){
        content_On_off = '<p class="text-online">ONLINE</p>';
      }else{
        content_On_off = '<p class="text-offline">OFFLINE</p>';
      }
      var show_data_time;
      if (!isNaN(new Date(val[i].timestamp).getHours()) || !isNaN(new Date(val[i].timestamp).getDate())){
        // show_data_time = '<td id = "show_time_timestamp'+val[i].id+'" >วันที่ ' + new Date(val[0].timestamp).getDate() +" เดือน "+strMonthCut[new Date(val[0].timestamp).getMonth()]+ ' '+ (new Date(val[0].timestamp).getFullYear() + 543) + ' เวลา : ' + new Date(val[i].timestamp).getHours()+":"+new Date(val[i].timestamp).getMinutes()+":"+new Date(val[i].timestamp).getSeconds()+" น."+'</td>'

        //console.log(moment(val[0].timestamp).add(-7,"hours").format('DD-MMMM-YYYY HH:mm:ss'));



        if (param_get_timezone == 0){
          show_data_time = '<td id = "show_time_timestamp'+val[i].id+'" >' + moment(val[0].timestamp).add(-7,"hours").format('DD-MMMM-YYYY HH:mm:ss') +'</td>'
        }else if (param_get_timezone == 7){
          show_data_time = '<td id = "show_time_timestamp'+val[i].id+'" >' + moment(val[0].timestamp).format('DD-MMMM-YYYY HH:mm:ss') +'</td>'
        }



        // show_data_time = '<td id = "show_time_timestamp'+val[i].id+'" >' + moment(val[0].timestamp).add(-7,"hours").format('DD-MMMM-YYYY HH:mm:ss') +'</td>'
        // show_data_time = '<td id = "show_time_timestamp'+val[i].id+'" >' + new Date(val[0].timestamp  ).getDate() +" "+monthNames[new Date(val[0].timestamp).getMonth()]+ ' '+ (new Date(val[0].timestamp).getFullYear()) + ' TIME : ' + new Date(val[i].timestamp).getHours()+":"+new Date(val[i].timestamp).getMinutes()+":"+new Date(val[i].timestamp).getSeconds()+'</td>'
      }else{
        show_data_time = '<td id = "show_time_timestamp'+val[i].id+'" >-</td>'; 
      }
      $("#device_table_body").append(
        '<tr id = "map_'+val[i].id+'">'+
          '<td>'+
            '<div class="custom-control custom-checkbox">'+
              '<input type="checkbox" class="custom-control-input" id="checked'+i+'" name="checked'+i+'" value="'+val[i].id+'">'+
              '<label class="custom-control-label" for="checked'+i+'"></label>'+
            '</div>'+
          '</td>'+
          // '<td>'+(i+1)+'</td>'+
          '<td>'+val[i].id+'</td>'+
          '<td>'+val[i].info+'</td>'+
          '<td id = "lat_'+val[i].id+'">'+val[i].lat+'</td>'+
          '<td id = "long_'+val[i].id+'">'+val[i].long+'</td>'+
          show_data_time+
          // '<td>'+val[i].port+'</td>'+ 
          '<td id = "content_On_off'+val[i].id+'">'+
            content_On_off+
          '</td>'+
          '<td id = "stream'+val[i].id+'">'+
            ((val[i].stream == 0) ? '<p class="text-inactive">INACTIVE</p>' : '<p class="text-active">ACTIVE</p>')+
          '</td>'+
          // '<td>'+view+'</td>'+
          // view+
          "<td hidden='hidden'>"+i+"</td>"+
          "<td>" +batt_level_level(val[i].batt)+ "</td>"+
          "<td id = 'gps_"+val[i].id+"'>"+ ((val[i].gps_count != undefined) ? val[i].gps_count : "0") +"</td>"+
          "<td><button class='btn btn-primary' style = 'font-size: 10px;' onclick = 'edit_edit_edit(\""+val[i].id+"\")'>EDIT</button></td>"+
        '</tr>'
      );

    }

    var table_d = $("#device_table").DataTable({
      scrollY: "500px",
      // ordering: false,
      paging: true,
      destroy: true,
      dom: 'Bfrtip',
      lengthChange: true,
      lengthMenu: [[-1], [ "All"]],
      buttons: [
        {
          extend: 'csv',
          title: "LIST_DEVICES",
          text: 'EXPORT DEVICE',
          exportOptions: {
            columns: [ 1, 2, 5 ],
            modifier: {
              search: 'none',
              columns: [ 1, 2, 3 ]
            }
          }
        }
      ]
    });

    // check_active_and_inactive();
    ininiiiiii(getCookie('device_now'));
    init_menu_config(val[0]);


    maps = new google.maps.Map(document.getElementById('map_device'), {
      zoom: 10,
      mapTypeId:  google.maps.MapTypeId.HYBRID,
      center: {
        lat: 13.751766, lng: 100.529284
    }});

    for (i = 0; i < val.length; i++) { 
      
      image = {				
        url:'../img/new2_mark.png',
      };

      var deferent = (Math.floor(new Date().getTime()/1000.0 - new Date(val[i].timestamp).getTime()/1000.0));
      $("#index").val(val.length);
      var content_On_off = ""
      if (deferent <= 60){
        content_On_off = '<p class="text-success"><i class="fas fa-circle text-success"></i> ONLINE</p>';
      }else{
        content_On_off = '</i><p class="text-muted"><i class="fas fa-circle text-muted"></i> OFFLINE</p>';
      }
      
      content_in_map.push('<div class="pinName" style="color:black"><b>'+content_On_off+'</b></div><hr><div class="pinName" style ="color:black"><b>Device: </b>'+val[i].id+'<b><br>Longitude:</b> '+val[i].long+'<br><b>Latitude:</b>'+val[i].lat+'</div>')
    }
    
    for (i = 0; i < val.length; i++) { 
      var myLatLng = {lat: parseFloat(val[i].lat), lng: parseFloat(val[i].long)};

      marker = new google.maps.Marker({
        position: new google.maps.LatLng(myLatLng.lat, myLatLng.lng),
        map: maps,
        title: "DEIVCE ID: " + val[i].id,
        icon: image,
      });

      info = new google.maps.InfoWindow();

      $("#map_"+val[i].id).click( (function(marker, i, val) {
        return function(){

          window.position_count = i;  

          var deferent = (Math.floor(new Date().getTime()/1000.0 - new Date(val[i].timestamp).getTime()/1000.0));

          if (deferent <= 60){
            content_On_off = '<p class="text-success"><i class="fas fa-circle text-success"></i> ONLINE</p>';
          }else{
            content_On_off = '</i><p class="text-muted"><i class="fas fa-circle text-muted"></i> OFFLINE</p>';
          }
          
          // content_in_map.push('<div class="pinName" style="color:black"><b>'+content_On_off+'</b></div><hr><div class="pinName" style ="color:black"><b>Device: </b>'+val[i].id+'<b><br>Longitude:</b> '+val[i].long+'<br><b>Latitude:</b>'+val[i].lat+'</div>')
          var out = '<div class="pinName" style="color:black"><b>'+content_On_off+'</b></div><hr><div class="pinName" style ="color:black"><b>Device: </b>'+val[i].id+'<b><br>Longitude:</b> '+val[i].long+'<br><b>Latitude:</b>'+val[i].lat+'<br><b>Battery:</b> '+ parseInt((val[i].batt * 100 / 7.5)) +'%</div>';
          info.setContent(out);
          info.open(maps, marker);
        }
      }) (marker, i, val) );
      google.maps.event.addListener(marker, 'click', (function(marker, i, val) {
      return function() {

        window.position_count = i;  


        var deferent = (Math.floor(new Date().getTime()/1000.0 - new Date(val[i].timestamp).getTime()/1000.0));

        if (deferent <= 60){
          content_On_off = '<p class="text-success"><i class="fas fa-circle text-success"></i> ONLINE</p>';
        }else{
          content_On_off = '</i><p class="text-muted"><i class="fas fa-circle text-muted"></i> OFFLINE</p>';
        }
        // content_in_map.push('<div class="pinName" style="color:black"><b>'+content_On_off+'</b></div><hr><div class="pinName" style ="color:black"><b>Device: </b>'+val[i].id+'<b><br>Longitude:</b> '+val[i].long+'<br><b>Latitude:</b>'+val[i].lat+'</div>')
        var out = '<div class="pinName" style="color:black"><b>'+content_On_off+'</b></div><hr><div class="pinName" style ="color:black"><b>Device: </b>'+val[i].id+'<b><br>Longitude:</b> '+val[i].long+'<br><b>Latitude:</b>'+val[i].lat+'<br><b>Battery:</b> '+ parseInt((val[i].batt * 100 / 7.5)) +'%</div>';
        info.setContent("");
        info.open(maps, marker);
      }})(marker, i, val));  

    }

  });
}

function input_date() {

  // select date RAW
  // $('#start_date_row').datetimepicker({
  //   timepicker:true,
  //   format:'Y-m-d H:i:s'
  // });
  $('#end_date_row').datetimepicker({
    timepicker:true,
    format:'Y-m-d H:i:s'
  });

  // select date Sise nav
  // $('#start_date').datetimepicker({
  //   timepicker:false,
  //   format:'d-m-Y'
  // });
  // $('#end_date').datetimepicker({
  //   timepicker:false,
  //   format:'d-m-Y'
  // });

}

function btn_start() {
  // page 1
  $('#GCH1').click();
  $('#GCH2').click();
  $('#GCH3').click();
  $('#map_box').click();
  $('#map_box1').click();
  $('#batt_box').click();
  $('#temp_box').click();
  $('#temper').text(30);

  // page 2
  $('#GROW').click();

  $('#search').click(function() {
    //get_row(time_set($('#start_date_row').val()),time_set($('#end_date_row').val()));
  });

  $('#record_data').click(function() {
    $('#sent_data').click();
  });

}



function logout(){
	// Submit
	try {
    $("#logout").click( function (){
      window.location.replace("../index.html");
    });
	} catch (e) {
		console.log(e);
	}

}


function delete_device(){

	try {
    $("#delete_device").click( function (){
      // console.log(this);
      var r = confirm("You want to delete the device.");
      var device___id = getCookie("device_now");
      var device_obj = JSON.parse(device___id)
      console.log(device_obj)
      if (r == true){
        var device = [];
        for (var i = 0;i < device_obj.length;i++){
          if (device_obj[i] != null){
            var settings = {
              "async": true,
              "crossDomain": true,
              "url": "http://128.199.218.27:3000/api/device",
              "method": "DELETE",
              "headers": {
                "Content-Type": "application/json",
                "X-Requested-With": "XMLHttpRequest",
                "Accept": "*/*",
                "Cache-Control": "no-cache",
                "cache-control": "no-cache"
              },
              "processData": false,
              "data": "{\n\t\"id\":\""+device_obj[i]+"\"\n}"
            }
            console.log(settings);
            $.ajax(settings).done(function (response) {
              console.log(response);
              setCookie("device_now","[]",30)
              location.reload();
            });
          }
        }
      }
      // console.log(device)
    });
	} catch (e) {
		console.log(e);
	}

}

function modal_close(){
	try {
    $("#modal_close").click( function (){
      // $("#text_device").val("") ;
      $("#description_device").val("");
    });
    $("#modal_close_edit").click( function (){
      // $("#text_device").val("") ;
      $("#description_device_edit").val("");
    });
	} catch (e) {
		console.log(e);
	}

}

function modal_confirm(){
	try {
    $("#modal_confirm").click( function (){
      // console.log('{"device":{"id":"'+$("#text_device").val()+'"}}');
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://128.199.218.27:3000/api/device/add",
        "method": "POST",
        "headers": {
          "Content-Type": "application/json",
          "X-Requested-With": "XMLHttpRequest",
          "Authorization": "Token <?php echo $_SESSION['token'];?>"
        },
        "processData": false,
        "data": '{"device":{"info":"'+$("#description_device").val()+'"}}'

        //"data": '{"device":{"id":"'+$("#text_device").val()+'","des":"'+$("#description_device").val()+'"}}'
      }
      // console.log(settings);
      $.ajax(settings).done(function (response) {
        // console.log(response);
        alert("INPUT NEW DEVICE SUCCESS.");
        location.reload();
      });
      $('#exampleModal').modal('hide')
      $("#text_device").val("")
    });
  } catch (e) {
    console.log(e);
  }
}


function record_data(){
	try {
    $("#record_data").click( function (){
      // $('#record_data').attr("disabled", true);
      alert("Start Record");
      var device = getCookie("device_now");
      var device_obj = JSON.parse(device)
      var new_arr = [];
      for (var i = 0;i < device_obj.length;i++){
        if (device_obj[i] != null){
          new_arr.push(device_obj[i])
        }
      }
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://128.199.218.27:3000/api/ssmic/start",
        "method": "POST",
        "headers": {
          "Content-Type": "application/json",
          "Accept": "*/*",
          "Cache-Control": "no-cache"
        },
        "processData": false,
        "data": "{ \"id\":"+JSON.stringify(new_arr)+"}"
      }
      console.log(settings);
      $.ajax(settings).done(function (response) {
        console.log(response);
      });
      // serin = setInterval(function(){   
      // try{
      //   get_realtime(); 
      // }catch(err) {
      //   console.log(err); 
      // }
      // }, 1000);
        
    });
	} catch (e) {
		console.log(e);
	}
}

function stop_data(){
	try {
    $("#stop_data").click( function (){
      $('#record_data').prop('disabled', false);
      // clearInterval(serin);
      alert("Stop Record");
      var device = getCookie("device_now");
      var device_obj = JSON.parse(device)
      var new_arr = [];
      for (var i = 0;i < device_obj.length;i++){
        if (device_obj[i] != null){
          new_arr.push(device_obj[i])
        }
      }
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://128.199.218.27:3000/api/ssmic/stop",
        "method": "POST",
        "headers": {
          "Content-Type": "application/json",
          "Accept": "*/*",
          "cache-control": "no-cache"
        },
        "processData": false,
        "data": "{ \"id\":"+JSON.stringify(new_arr)+"}"
      }
      console.log(settings)
      $.ajax(settings).done(function (response) {
        console.log(response);
      });
    });
	} catch (e) {
		console.log(e);
	}
}

function init_menu_config(val){
  //var val=response.reverse();
  var deferent = (Math.floor(new Date().getTime()/1000.0 - new Date(val.timestamp).getTime()/1000.0));
  $('#side_id').text(val.id);
  if (deferent <= 60){
    $('#side_status').text("ONLINE");
  }else{
    $('#side_status').text("OFFLINE");
  }
}

function open_dashboard(){
  $('#open_dashobard').click(function() {
    var device = [];
    for (var i = 0;i < data_table_json.length;i++){
      if ($("#checked"+i).prop('checked')){
        device.push($("#checked"+i).val());
        // console.log(device)
      }
    }
    if (device.length > 0){
      window.open('./view_chart.php?device='+JSON.stringify(device) + "&token=<?php echo $_SESSION['token']; ?>&email=<?php echo $_SESSION["email"];?>", '_blank');
    }else{
      alert("Select Device");
    }
  });
}

function check_status(){
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://128.199.218.27:3000/api/device",
    "method": "GET"
  }

  $.ajax(settings).done(function (response) {
    var val=response;
    console.log(val);
    // show_time_timestamp content_On_off stream
    for (var i = 0;i < val.length;i++){
      $("#lat_"+val[i].id).text(val[i].lat)
      $("#long_"+val[i].id).text(val[i].long)
      $("#gps_"+val[i].id).text(((val[i].gps_count != undefined) ? val[i].gps_count : "0"))
      
      var deferent = (Math.floor(new Date().getTime()/1000.0 - new Date(val[i].timestamp).getTime()/1000.0));
      var content_On_off = ""
      if (deferent <= 60){
        content_On_off = '<p class="text-online">ONLINE</p>';
      }else{
        content_On_off = '<p class="text-offline">OFFLINE</p>';
      }
      $("#content_On_off"+val[i].id).html("");
      $('#content_On_off'+val[i].id).append(content_On_off);
      var show_data_time;
      if (!isNaN(new Date(val[i].timestamp).getHours()) || !isNaN(new Date(val[i].timestamp).getDate())){
        // show_data_time = ' ' + new Date(val[0].timestamp).getDate() +" "+monthNames[new Date(val[0].timestamp).getMonth()]+ ' '+ (new Date(val[0].timestamp).getFullYear()) + '  TIME : ' + new Date(val[i].timestamp).getHours()+":"+new Date(val[i].timestamp).getMinutes()+":"+new Date(val[i].timestamp).getSeconds()+""
        if (param_get_timezone == 0){
          show_data_time = moment(val[0].timestamp).add(-7,"hours").format('DD-MMMM-YYYY HH:mm:ss')
        }else if (param_get_timezone == 7){
          show_data_time = moment(val[0].timestamp).format('DD-MMMM-YYYY HH:mm:ss')
        }

      }else{
        // show_data_time = 'วันที่ - เดือน - เวลา : -:-:- น.'; 
        show_data_time = "-"
      }
      $("#show_time_timestamp"+val[i].id).text(show_data_time);
      var content_out_stream = ((val[i].stream == 0) ? '<p class="text-inactive">INACTIVE</p>' : '<p class="text-active">ACTIVE</p>')
      $("#stream"+val[i].id).html("");
      $('#stream'+val[i].id).append(content_out_stream);  

      console.log("window.position_count:: "+window.position_count)

      var myLatLng = {lat: parseFloat(val[i].lat), lng: parseFloat(val[i].long)};
      var deferent = (Math.floor(new Date().getTime()/1000.0 - new Date(val[i].timestamp).getTime()/1000.0));
      if (deferent <= 60){
        content_On_off = '<p class="text-success"><i class="fas fa-circle text-success"></i> ONLINE</p>';
      }else{
        content_On_off = '</i><p class="text-muted"><i class="fas fa-circle text-muted"></i> OFFLINE</p>';
      }
      content_in_map.push('<div class="pinName" style="color:black"><b>'+content_On_off+'</b></div><hr><div class="pinName" style ="color:black"><b>Device: </b>'+val[i].id+'<b><br>Longitude:</b> '+val[i].long+'<br><b>Latitude:</b>'+val[i].lat+'</div>')
      var out = '<div class="pinName" style="color:black"><b>'+content_On_off+'</b></div><hr><div class="pinName" style ="color:black"><b>Device: </b>'+val[i].id+'<b><br>Longitude:</b> '+val[i].long+'<br><b>Latitude:</b>'+val[i].lat+'<br><b>Battery:</b> '+ parseInt((val[i].batt * 100 / 7.5)) +'%</div>';

      if (window.position_count == i){
        info.setContent(out);
      }
    }


  });
}

// cookie here~

function setCookie(cname,cvalue,exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires=" + d.toGMTString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "[]";
}

function checkCookie() {
  var user=getCookie("username");
  if (user != "") {
    alert("Welcome again " + user);
  } else {
     user = prompt("Please enter your name:","");
     if (user != "" && user != null) {
      //  setCookie("username", user, 30);
     }
  }
}

function ininiiiiii(cook){
  $("#select_device_show").html("");
  var dev_obj = JSON.parse(cook)
  for (var i = 0;i < dev_obj.length;i++){
    if (dev_obj[i] != null){
      console.log("#checked" + i)
      $("#checked" + i ).prop('checked',true)
      $("#select_device_show").append(
        "<tr>"+
          "<td>"+ dev_obj[i] +"</td>"+
        "</tr>"
      );
    }
  }
}


function check_active_and_inactive(){
  var arr = []
  var td = $("#device_table_body").find('tr').length
  for (var i = 0;i< $("#device_table_body").find('tr').length;i++){
    if ($("#device_table_body").find('tr').find('td').eq(7).text() == 'ACTIVE'){
      $("#checked" + i ).prop('checked',true)
      arr[i] = $("#device_table_body").find('tr').find('td').eq(2).text()
    }
  }
  console.log(arr)
  setCookie("device_now",JSON.stringify(arr),30) // cook exp mounth.
}


// this is check inactive device [ recorded ] 
function link_fc_read_time_page(){
  try{
    var device = getCookie("device_now");
    var device_obj = JSON.parse(device)
    var check = false;
    for (var i = 0;i < device_obj.length;i++){
      if (device_obj[i] != null ){
        window.location.replace("../source/view_chart.php?device=[\"" + device_obj[i] + "\"]" + "&timezone=<?php echo $_GET['timezone'];?>");
        check = true;
        break;
      }
    }
    if (!check){
      alert("Please select device");
    }
  } catch (err) {

  }
}

function batt_level_level(batt_level_value){
  batt_level_value = parseInt((batt_level_value * 100 / 7.5))
  var content_batt = "";
  if (batt_level_value <= 5){
    content_batt = '<i class="fas fa-battery-empty" style = "color: #e1737d;font-size: 20px;"></i><br>'+batt_level_value+'%'
  }else if (batt_level_value <= 25){
    content_batt = '<i class="fas fa-battery-quarter" style = "color: #f6b806;font-size: 20px;"></i><br>'+batt_level_value+'%'
  }else if (batt_level_value <= 50){
    content_batt = '<i class="fas fa-battery-half" style = "color: #d0e61a;font-size: 20px;"></i><br>'+batt_level_value+'%'
  }else if (batt_level_value <= 75){
    content_batt = '<i class="fas fa-battery-three-quarters" style = "color: #289b4c;font-size: 20px;"></i><br>'+batt_level_value+'%'
  }else if (batt_level_value <= 100){
    content_batt = '<i class="fas fa-battery-full" style = "color: #1ba647;font-size: 20px;"></i><br>'+batt_level_value+'%'
  }else if (batt_level_value == undefined){
    content_batt = "-"
  }
  return content_batt
}

function modal_confirm_edit(){
	try {
    $("#modal_confirm_edit").click( function (){
      // console.log('{"device":{"id":"'+$("#text_device").val()+'"}}');
      var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://128.199.218.27:3000/api/device/update",
        "method": "POST",
        "headers": {
          "Content-Type": "application/json",
          "X-Requested-With": "XMLHttpRequest",
          "Authorization": "Token <?php echo $_SESSION['token'];?>"
        },
        "processData": false,
        "data": "{\"id\":\""+$("#device_id_for_edit").val()+"\",\"info\":\""+$("#description_device_edit").val()+"\"}"
      }
      console.log(settings);
      $.ajax(settings).done(function (response) {
        console.log(response);
        if (response.ret == "OK"){
          alert("Success")
          location.reload();
        }
      });
      $('#exampleModal1').modal('hide')
      $("#description_device_edit").val("")
    });
  } catch (e) {
    console.log(e);
  }
}
function edit_edit_edit(device_id){
  $('#device_id_for_edit').val(device_id)
  $('#exampleModal1').modal('show')
}


function select_all_all(){
  $("#select_all").click(function(){
    var arr = []
    var td = $("#device_table_body").find('tr').length
    console.log($("#device_table_body").find('tr').find('td'));
    for (var i = 1;i < $("#device_table_body").find('tr').find('td').length;i+=12){
      console.log(i)
      // arr[] = $("#device_table_body").find('tr').find('td')[i].textContent
      arr.push($("#device_table_body").find('tr').find('td')[i].textContent)
      
    }
    $("#select_device_show").html("")
    for (var i = 0;i< $("#device_table_body").find('tr').length;i++){
      $("#checked" + i ).prop('checked',true)
      $("#select_device_show").append(
        "<tr>"+
          "<td>"+ arr[i] +"</td>"+
        "</tr>"
      );
    }
    setCookie("device_now",JSON.stringify(arr),30) // cook exp mounth.
    // location.reload();
  });
}

function unselect_all_all(){
  $("#unselect_all").click(function(){
    $("#select_device_show").html("")
    var arr = [];
    setCookie("device_now",JSON.stringify(arr),30) // cook exp mounth.
    for (var i = 0;i < $("#device_table_body").find('tr').length;i++){
      $("#checked" + i ).prop('checked',false)
    }
  });
}

$( document ).ready(function() {
  show_event_table();

  $('#stop_date_event_time').datetimepicker({
    timepicker:true,
    format:'Y-m-d H:i:s'
  });

  $('#start_date_event_time').datetimepicker({
    timepicker:true,
    format:'Y-m-d H:i:s'
  });

  button_add_event();
  button_delete_event();
  button_add_device_to_event();
});

function show_event_table(){
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://128.199.218.27:3000/api/event",
    "method": "GET",
  }

  $.ajax(settings).done(function (response) {
    console.log(response);
    $("#table_body_event").html("");
    var content_output =  "";
    $("#SELECT_EVENT_ID").html("");
    $("#SELECT_EVENT_ID").append("<option selected='' value='0'>SELECT EVENT</option>");
    for (var i = 0;i < response.length;i++){
      $("#SELECT_EVENT_ID").append("<option value='"+response[i].id+"'>"+response[i].id+"</option>");
      content_output += "<tr>"
        content_output += "<td><center>"
          content_output += "<div class='custom-control custom-checkbox'>"
            content_output += "<input type='checkbox' class='custom-control-input' id='checked_event_"+i+"' name='checked_event_"+i+"' value=''>"
            content_output += "<label class='custom-control-label' for='checked_event_"+i+"'></label>"
          content_output += "</div>"
        content_output += "</center></td>"

        content_output += "<td id = 'event_id_"+i+"'><center>"+response[i].id+"</center></td>"
        // var show_data_time_start = ' ' + new Date(response[i].start).getDate() +" "+monthNames[new Date(response[i].start).getMonth()]+ ' '+ (new Date(response[i].start).getFullYear()) + '  TIME : ' + new Date(response[i].start).getHours()+":"+new Date(response[i].start).getMinutes()+":"+new Date(response[i].start).getSeconds()+""
        // var show_data_time_end = ' ' + new Date(response[i].stop).getDate() +" "+monthNames[new Date(response[i].stop).getMonth()]+ ' '+ (new Date(response[i].stop).getFullYear()) + '  TIME : ' + new Date(response[i].stop).getHours()+":"+new Date(response[i].stop).getMinutes()+":"+new Date(response[i].stop).getSeconds()+""
        if (param_get_timezone == 0){
          content_output += "<td><center>"+moment(response[i].start,'YYYY-MM-DD HH:mm:ss').add(-7,"hours").format('DD-MMMM-YYYY HH:mm:ss')+"</center></td>"
          content_output += "<td><center>"+moment(response[i].stop,'YYYY-MM-DD HH:mm:ss').add(-7,"hours").format('DD-MMMM-YYYY HH:mm:ss')+"</center></td>" 
        }else if (param_get_timezone == 7){
          content_output += "<td><center>"+moment(response[i].start,'YYYY-MM-DD HH:mm:ss').format('DD-MMMM-YYYY HH:mm:ss')+"</center></td>"
          content_output += "<td><center>"+moment(response[i].stop,'YYYY-MM-DD HH:mm:ss').format('DD-MMMM-YYYY HH:mm:ss')+"</center></td>" 
        }

        // content_output += "<td><center>"+show_data_time_start+"</center></td>"
        // content_output += "<td><center>"+show_data_time_end+"</center></td>" 
        content_output += "<td><center>";
        for (var m = 0;m < response[i].unit.length;m++){
          content_output += response[i].unit[m] 
          if (!(m == response[i].unit.length - 1)){
            content_output += " - "
          }
          // content_output += "<span class='font-size-18 badge badge-primary'>"+response[i].unit[m]+"</span><br><hr>"
        }
        content_output += "</center></td>";
        // content_output += "<td>"+response[i].id+"</td>"
      content_output += "</tr>"
    }
    $("#table_body_event").append(content_output);

    var table_d = $("#row_table_event").DataTable({
      scrollY: "500px",
      // ordering: false,
      paging: true,
      destroy: true,
      lengthChange: true,
      lengthMenu: [[-1], [ "All"]],
    });


  });
}

function button_delete_event(){
  $("#delete_device_event").click(function(){
    var table = $("#table_body_event");
    var $tds = $(table).find('td');
    var id = $tds.eq(1).text();
    var status = $tds.eq(6).text();
    console.log($tds.length / 5)
    for (var i = 0;i < ($tds.length / 5);i++){
      console.log($("#checked_event_" + i).prop('checked'))

      if ($("#checked_event_" + i).prop('checked') == true){
        var settings = {
          "async": true,
          "crossDomain": true,
          "url": "http://128.199.218.27:3000/api/event",
          "method": "DELETE",
          "headers": {
            "Content-Type": "application/json",
            "User-Agent": "PostmanRuntime/7.19.0",
            "Accept": "*/*",
            "Cache-Control": "no-cache",
            "Postman-Token": "342efd2d-25a1-4559-a300-aab16f2a3cc3,800c796e-31d9-4d0a-b9a7-463382f5e7e0",
            "Host": "128.199.218.27:3000",
            "Accept-Encoding": "gzip, deflate",
            "Content-Length": "23",
            "Connection": "keep-alive",
            "cache-control": "no-cache"
          },
          "processData": false,
          "data": "{\r\n\t\"id\":\""+$("#event_id_"+i).text()+"\"\r\n}"
        }
        console.log(settings)
        $.ajax(settings).done(function (response) {
          console.log(response);
          location.reload();
        });
      }



    }
    // console.log($("#checked_event_0").prop('checked'))
    // if ($("#checked" + $tds.eq(8).text() ).prop('checked')){
    // }


  });
}

function button_add_event(){
  $("#modal_confirm_event").click(function (){
    console.log($("#stop_date_event_time").val())
    console.log($("#start_date_event_time").val())

    var settings = {
      "async": true,
      "crossDomain": true,
      "url": "http://128.199.218.27:3000/api/event",
      "method": "POST",
      "headers": {
        "Content-Type": "application/json",
        "Accept": "*/*",
      },
      "processData": false,
      "data": "{\r\n    \"start\":\""+$("#start_date_event_time").val()+"\",\r\n    \"stop\":\""+$("#start_date_event_time").val()+"\"\r\n}"
    }

    $.ajax(settings).done(function (response) {
      console.log(response);
      if (response.ret == "ok"){
        location.reload();
      }
    });



  })
}

function button_add_device_to_event(){
  $("#modal_confirm_add_device_to_event").click(function(){
    var arr_out = []
    var new_arr = [];
    try{
      arr_out = JSON.parse(getCookie('device_now'));
      var device = $("#SELECT_EVENT_ID").val();
      for (var i = 0;i < arr_out.length;i++){
        if (arr_out[i] != null){
          new_arr.push(arr_out[i])
        }
      }
      console.log(new_arr);
      console.log(device)
      if (device != 0){
        var settings = {
          "async": true,
          "crossDomain": true,
          "url": "http://128.199.218.27:3000/api/event/update",
          "method": "POST",
          "headers": {
            "Content-Type": "application/json",
            "User-Agent": "PostmanRuntime/7.19.0",
            "Accept": "*/*",
            "Cache-Control": "no-cache",
            "Postman-Token": "c2560a87-f914-4e32-ac7c-256cf77f90f2,a88c6cdb-d121-4ba1-9810-73264a0a18af",
            "Host": "128.199.218.27:3000",
            "Accept-Encoding": "gzip, deflate",
            "Content-Length": "109",
            "Connection": "keep-alive",
            "cache-control": "no-cache"
          },
          "processData": false,
          "data": "{\r\n\t\"id\":\""+device+"\",\r\n\r\n\t\"unit\":"+JSON.stringify(new_arr)+"\r\n}"
        }
        console.log(settings);
        $.ajax(settings).done(function (response) {
          console.log(response);
          location.reload();
        });
      }
    }catch(err){
      console.log(err);
    }
  });
}
function get_timezone(){
  // 1 = UTC
  // 2 = GMT
  if ($("#select_timezone").val() != "0"){
    console.log($("#select_timezone").val());
    switch($("#select_timezone").val()) {
      case "1":
        windown_get_param("0")
        break;
      case "2":
        windown_get_param("7")
        break;
      default:
        console.log("not OK not")
    }
  }else{
    alert("Please select Timezone")
  }
}
function windown_get_param(value){
  var url = new URL(window.location.href);
  url.searchParams.set('timezone',value);
  window.location.href = url.href;
}
</script>


</body>
</html>




<!--  
  battery level is icon

  0%      = <i class="fas fa-battery-empty"></i>              <= 5
  25%     = <i class="fas fa-battery-quarter"></i>            <= 25
  50%     = <i class="fas fa-battery-half"></i>               <= 50
  75%     = <i class="fas fa-battery-three-quarters"></i>     <= 75
  100%    = <i class="fas fa-battery-full"></i>               <= 100
-->