// page 2
function graph_row(graph_x,graph_ch1_y,graph_ch2_y,graph_ch3_y) {
  var start = +new Date();
  Highcharts.chart('row_coll', {
    
    chart: {
      type: 'spline',
      zoomType: 'x',
      events: {
        load: function () {
          if (!window.TestController) {
            this.setTitle(null, {
              text: 'Built chart in ' + (new Date() - start) + 'ms'
            });
          }
        }
      }
    },
    boost: {
      useGPUTranslations: true
  },
    title: {
      text: 'Channel'
    },
    subtitle: {
      text: 'Built chart in ...'
    },
    credits: {
      enabled: false
    },
    xAxis: {
      categories: graph_x
    },
    yAxis: {
      title: {
        text: 'VOLTAGE (V)'
      }
    },
    plotOptions: {
      series: {
        allowPointSelect: true
      }
    },
    series: [
              {
                name: "CH1",
                data: graph_ch1_y
              },
              {
                name: "CH2",
                data: graph_ch2_y
              },
              {
                name: "CH3",
                data: graph_ch3_y
              }
            ]
  });
}

function get_row(time_s,time_e,period) {
  // test
  // time_e = 1553141092000;
  // time_s = 1553141060000;
  // into table



  // console.log('..get_raw..');
  // console.log(time_s);
  // console.log(time_e);
  // time_e = 1553141092000;
  // time_s = 1553141060000;
    try {
      //table_s.destroy();
      $(".head_table").empty();
      $('.head_table').append(
        '<table class="table table-hover table-bordered" id="row_table">'+
        '<thead>'+
        '<tr class="bg-primary" align= "center">'+



          '<th>#</th>'+
          '<th>Start Time</th>'+
          '<th>End Time</th>'+
          '<th hidden></th>'+
          '<th hidden></th>'+
          '<th hidden></th>'+
          
          '<th hidden></th>'+
          '<th hidden></th>'+




        '</tr>'+
        '</thead>'+
        '<tbody id="table_body">'+
  
        '</tbody>'+
        '</table>'+
        '<center><div class="loader" id="row_load2"></div></center>'
      );
      $("#table_body").empty();
    } catch (e) {
      console.log(e);
    }



    // for table here!
    axios({
      method: 'get',
      // url: 'http://128.199.218.27:3000/api/data?id='+device_id+'&st='+time_s.replace(" ","T")+'&ed='+time_e.replace(" ","T"),
      url: 'http://128.199.218.27:3000/api/data/index?id='+device_id+'&period='+period+'&st='+time_s.replace(" ","T")+'&ed='+time_e.replace(" ","T"),
    })
    .then(function (response) {
      // console.log(response.data.t_index)
      create_table_new(response.data)
    })
    .catch(function (err) {
    //document.getElementById('people').innerHTML = '<li class="text-danger">' + err.message + '</li>';
    });








  //   var settings = {
  //     "async": true,
  //     "crossDomain": true,
  //     "url": "http://128.199.218.27:3000/api/data?id="+device_id+"&st="+time_s.replace(" ","T")+"&ed="+time_e.replace(" ","T"),
  //     "method": "GET"
  //   }
  //   console.log(settings)
  //   $.ajax(settings).done(function (response) {


  //     console.log(response)
  // // --------------------------------------------------------------------------------------
  //     try {
  //       // console.log(response[0].payload);

  //       var graph_ch1_x=[],graph_ch1_y=[];
  //       var graph_ch2_x=[],graph_ch2_y=[];
  //       var graph_ch3_x=[],graph_ch3_y=[];

  //       var utc_plus=0;
  //       var utc_plus_val=0;

  //       for(var index=0;index<response.length;index++){
  //         var val = response[index].payload;

  //         utc_plus = response[index].UTC;
  //         utc_plus_val = Math.floor((1/response[index].sampling)*(1000));

  //         for(var index2=0;index2<val.length;index2++){

  //           for(var index3=0;index3<val[index2].data.length;index3++){

  //             $("#table_body").append(
  //               '<tr>'+
  //               '<td>'+response[index].id+'</td>'+
  //               '<td>'+val[index2].ch+'</td>'+
  //               '<td>'+utc_plus+'</td>'+
  //               '<td>'+moment(utc_plus).format("YYYY-MM-DDTH:mm:ss:SSS Z")+'</td>'+
  //               '<td>'+val[index2].data[index3]+'</td>'+
  //               '</tr>'
  //             );

  //             switch(val[index2].ch) {
  //               case 1:
  //               graph_ch1_x.push(moment(utc_plus).format("H:mm:ss:SSS"));
  //               graph_ch1_y.push(val[index2].data[index3]);
  //               break;
  //               case 2:
  //               graph_ch2_x.push(moment(utc_plus).format("H:mm:ss:SSS"));
  //               graph_ch2_y.push(val[index2].data[index3]);
  //               break;
  //               case 3:
  //               graph_ch3_x.push(moment(utc_plus).format("H:mm:ss:SSS"));
  //               graph_ch3_y.push(val[index2].data[index3]);
  //               break;
  //               default:
  //               console.log("Empty Value!!");
  //             }
  //             utc_plus+=utc_plus_val;
  //           }
  //         }
  //         utc_plus=0;
  //       }
  //     } catch (e) {
  //       console.log(e);
  //     } finally {
  //       graph_row(graph_ch1_x,graph_ch1_y,graph_ch2_y,graph_ch3_y)
  //     }
  // // --------------------------------------------------------------------------------------
  //     table_s = $("#row_table").DataTable({
  //       destroy: true,
  //       dom: 'Bfrtip',
  //       buttons: [
  //         {
  //           extend: 'csv',
  //           text: 'CSV',
  //           title: "LIST_DATA",
  //           exportOptions: {
  //             modifier: {
  //               search: 'none'
  //             }
  //           }
  //         }
  //       ]
  //     });
  //     $("#row_load2").hide();



  //   });
}


function this_create_chart(response){
  // --------------------------------------------------------------------------------------
  try {

    var graph_ch1_x=[],graph_ch1_y=[];
    var graph_ch2_x=[],graph_ch2_y=[];
    var graph_ch3_x=[],graph_ch3_y=[];

    var utc_plus=0;
    var utc_plus_val=0;

    for(var index=0;index<response.length;index++){
      var val = response[index].payload;

      utc_plus = response[index].UTC;
      utc_plus_val = Math.floor((1/response[index].sampling)*(1000));

      for(var index2=0;index2<val.length;index2++){

        for(var index3=0;index3<val[index2].data.length;index3++){

          // $("#table_body").append(
          //   '<tr>'+
          //   '<td>'+response[index].id+'</td>'+
          //   '<td>'+val[index2].ch+'</td>'+
          //   '<td>'+utc_plus+'</td>'+
          //   '<td>'+moment(utc_plus).format("YYYY-MM-DDTH:mm:ss:SSS Z")+'</td>'+
          //   '<td>'+val[index2].data[index3]+'</td>'+
          //   '</tr>'
          // );

          switch(val[index2].ch) {
            case 1:
            graph_ch1_x.push(moment(utc_plus).add(param_get_timezone,"hours").format("H:mm:ss:SSS"));
            graph_ch1_y.push(val[index2].data[index3]);
            break;
            case 2:
            graph_ch2_x.push(moment(utc_plus).add(param_get_timezone,"hours").format("H:mm:ss:SSS"));
            graph_ch2_y.push(val[index2].data[index3]);
            break;
            case 3:
            graph_ch3_x.push(moment(utc_plus).add(param_get_timezone,"hours").format("H:mm:ss:SSS"));
            graph_ch3_y.push(val[index2].data[index3]);
            break;
            default:
            console.log("Empty Value!!");
          }
          utc_plus+=utc_plus_val;
        }
      }
      utc_plus=0;
    }
  } catch (e) {
    console.log(e);
  } finally {
    console.log(graph_ch1_x)
    graph_row(graph_ch1_x,graph_ch1_y,graph_ch2_y,graph_ch3_y)
  }
  // --------------------------------------------------------------------------------------
  // table_s = $("#row_table").DataTable({
  //   destroy: true,
  //   // dom: 'Bfrtip',
  //   // buttons: [
  //   //   {
  //   //     extend: 'csv',
  //   //     text: 'CSV',
  //   //     title: "LIST_DATA",
  //   //     exportOptions: {
  //   //       modifier: {
  //   //         search: 'none'
  //   //       }
  //   //     }
  //   //   }
  //   // ]
  // });
  // $("#row_load2").hide();
}


function create_table_new(data){
  console.log(data);
  for (var i = 0;i < data.d_group.length;i++){
    // console.log(data.d_group[i].data[0] +" " +data.d_group[i].data[data.d_group[i].data.length - 1])

    var date = moment(data.d_group[i].data[0],'YYYY-MM-DD HH:mm:ss');
    var a = moment(date,'YYYY-MM-DD HH:mm:ss');
    var b = moment(moment().format('YYYY-MM-DD HH:mm:ss'),'YYYY-MM-DD HH:mm:ss');


    $("#table_body").append(

      '<tr align = "center">'+
          
        '<td>'+
          '<div class="custom-control custom-checkbox" style = "padding-bottom: 20px !important;">'+
            '<input type="checkbox" class="custom-control-input" id="checkedraw'+i+'" name="checkedraw'+i+'" value="'+i+'">'+
            '<label class="custom-control-label" for="checkedraw'+i+'"></label>'+
          '</div>'+
        '</td>'+
        '<td onclick = "create_chart____________(\''+data.d_group[i].f_time+'\',\''+data.d_group[i].l_time+'\')">'+moment(data.d_group[i].f_time,'YYYY-MM-DD HH:mm:ss:SSS').add(param_get_timezone, 'hours').format('YYYY-MM-DD HH:mm:ss:SSS')  +'</td>'+
        '<td onclick = "create_chart____________(\''+data.d_group[i].f_time+'\',\''+data.d_group[i].l_time+'\')">'+moment(data.d_group[i].l_time,'YYYY-MM-DD HH:mm:ss:SSS').add(param_get_timezone, 'hours').format('YYYY-MM-DD HH:mm:ss:SSS')  +'</td>'+
        // '<td onclick = "create_chart____________(\''+moment(data.d_group[i].f_time,'YYYY-MM-DD HH:mm:ss:SSS').add(-7, 'hours').format('YYYY-MM-DDTHH:mm:ss:SSS')+'\',\''+moment(data.d_group[i].l_time,'YYYY-MM-DD HH:mm:ss:SSS').add(-7, 'hours').format('YYYY-MM-DDTHH:mm:ss:SSS')+'\')">'+moment(data.d_group[i].f_time,'YYYY-MM-DD HH:mm:ss:SSS').add(param_get_timezone, 'hours').format('YYYY-MM-DD HH:mm:ss:SSS')  +'</td>'+
        // '<td onclick = "create_chart____________(\''+moment(data.d_group[i].f_time,'YYYY-MM-DD HH:mm:ss:SSS').add(-7, 'hours').format('YYYY-MM-DDTHH:mm:ss:SSS')+'\',\''+moment(data.d_group[i].l_time,'YYYY-MM-DD HH:mm:ss:SSS').add(-7, 'hours').format('YYYY-MM-DDTHH:mm:ss:SSS')+'\')">'+moment(data.d_group[i].l_time,'YYYY-MM-DD HH:mm:ss:SSS').add(param_get_timezone, 'hours').format('YYYY-MM-DD HH:mm:ss:SSS')  +'</td>'+
        
        
        '<td hidden>'+device_id+'</td>'+
        '<td hidden>'+data.d_group[i].l_time+'</td>'+

        '<td hidden>'+i+'</td>'+

        '<td hidden>'+data.d_group[i].f_time+'</td>'+
        '<td hidden>'+data.d_group[i].l_time+'</td>'+
      '</tr>'




      // '<tr align = "center">'+
        
      //   '<td>'+
      //     '<div class="custom-control custom-checkbox" style = "padding-bottom: 20px !important;">'+
      //       '<input type="checkbox" class="custom-control-input" id="checkedraw'+i+'" name="checkedraw'+i+'" value="'+i+'">'+
      //       '<label class="custom-control-label" for="checkedraw'+i+'"></label>'+
      //     '</div>'+
      //   '</td>'+
        
      //   '<td onclick = "create_chart____________(\''+data.d_group[i].data[0]+'\',\''+data.d_group[i].data[data.d_group[i].data.length - 1]+'\')">'+moment(data.d_group[i].data[0],'YYYY-MM-DD HH:mm:ss:SSS').format('YYYY-MM-DD HH:mm:ss:SSS')  +'</td>'+

      //   '<td onclick = "create_chart____________(\''+data.d_group[i].data[0]+'\',\''+data.d_group[i].data[data.d_group[i].data.length - 1]+'\')">'+moment(data.d_group[i].data[data.d_group[i].data.length - 1],'YYYY-MM-DD HH:mm:ss:SSS').format('YYYY-MM-DD HH:mm:ss:SSS')  +'</td>'+

      //   '<td hidden>'+device_id+'</td>'+
      //   '<td hidden>'+data.d_group[i].data[data.d_group[i].data.length - 1]+'</td>'+

      //   '<td hidden>'+i+'</td>'+

      //   '<td hidden>'+data.d_group[i].data[0]+'</td>'+
      //   '<td hidden>'+data.d_group[i].data[data.d_group[i].data.length - 1]+'</td>'+

      // '</tr>'
    );
  }
  table_s = $("#row_table").DataTable({      
    scrollY: "500px",
    lengthMenu: [[-1], [ "All"]],
  });
  $("#row_load2").hide();
}


function create_chart____________(time_s,time_e,){
  // console.log(time_s + " " + time_e + device_id);
    // // for chart here!
    
    console.log("========= debug ====== data return ======== create_chart____________ =========")
    console.log("start date : " + time_s);
    console.log("end date   : " + time_e);
    console.log("==============================================================================")
    axios({
      method: 'get',
      url: 'http://128.199.218.27:3000/api/data?id='+device_id+'&st='+time_s+'&ed='+time_e,
      headers: {
        'Content-Type': 'application/javascript',
        // 'Authorization':'Token eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVjZTAxNjVkZGVmNzRjMDAyM2U3NGYzNyIsInVzZXJuYW1lIjoiYmFuZGlkMTIiLCJleHAiOjE1NjMzNzM4NTcsImlhdCI6MTU1ODE4OTg1N30.OFLQUNWQPn8jdtHTDSonAwtxFv9J9v9WZ8Z9bx5cuzc',
        'X-Requested-With':'XMLHttpRequest',
      }
    })
    .then(function (response) {
      console.log("====================")
      console.log(response.data)
      console.log("====================")
      this_create_chart(response.data)
    })
    .catch(function (err) {
    //document.getElementById('people').innerHTML = '<li class="text-danger">' + err.message + '</li>';
    });
}