

// page 1
function graph1(graph_x,graph_y) {
  var start = +new Date();
  Highcharts.chart('ch1', {
    chart: {
      type: 'spline',
      zoomType: 'x',
      events: {
        load: function () {
          if (!window.TestController) {
            this.setTitle(null, {
              text: 'Built chart in ' + (new Date() - start) + 'ms'
            });
          }
        }
      }
    },
    title: {
      text: 'CH1'
    },
    subtitle: {
      text: 'Built chart in ...'
    },
    boost: {
      useGPUTranslations: true
    },
    credits: {
      enabled: false
    },
    xAxis: {
      categories: graph_x
    },
    yAxis: {
      title: {
        text: 'VOLTAGE (V)'
      },
//      max: 0.5,
//      min: -0.5
    },
    plotOptions: {
      series: {
        animation: false,
        allowPointSelect: true
      }
    },
    series: [{
      data: graph_y
    }]
  });
}

function graph2(graph_x,graph_y) {
  var start = +new Date();
  Highcharts.chart('ch2', {
    chart: {
      type: 'spline',
      zoomType: 'x',
      events: {
        load: function () {
          if (!window.TestController) {
            this.setTitle(null, {
              text: 'Built chart in ' + (new Date() - start) + 'ms'
            });
          }
        }
      }
    },
    boost: {
      useGPUTranslations: true
  },
    title: {
      text: 'CH2'
    },
    subtitle: {
      text: 'Built chart in ...'
    },
    credits: {
      enabled: false
    },
    xAxis: {
      categories: graph_x
    },
    yAxis: {
      title: {
        text: 'VOLTAGE (V)'
      },
//      max: 0.5,
//      min: -0.5
    },
    plotOptions: {
      series: {
        animation: false,
        allowPointSelect: true
      }
    },
    series: [{
      data: graph_y
    }]
  });
}

function graph3(graph_x,graph_y) {
  console.log(graph_x)
  console.log(graph_y)
  var start = +new Date();
  Highcharts.chart('ch3', {
    chart: {
      type: 'spline',
      zoomType: 'x',
      events: {
        load: function () {
          if (!window.TestController) {
            this.setTitle(null, {
              text: 'Built chart in ' + (new Date() - start) + 'ms'
            });
          }
        }
      }
    },
    boost: {
      useGPUTranslations: true
  },
    title: {
      text: 'CH3'
    },
    subtitle: {
      text: 'Built chart in ...'
    },
    credits: {
      enabled: false
    },
    xAxis: {
      categories: graph_x
    },
    yAxis: {
      title: {
        text: 'VOLTAGE (V)'
      },
//      max: 0.5,
//      min: -0.5
    },
    plotOptions: {
      series: {
        animation: false,
        allowPointSelect: true
      }
    },
    series: [{
      data: graph_y
    }]
  });
}

function get_realtime(response,device_id_get_php) {
  console.log("=================== debug get_realtime=================")
  console.log(response);
  console.log("=================== debug get_realtime=================")
  var strMonthCut = ["ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."];
  var date = moment(response.STime,'YYYY-MM-DD HH:mm:ss:SSS').add(param_get_timezone,"hours");
  var data_text = "[ วันที่ " + date.format('DD') + " " + strMonthCut[date.format('MM') - 1] + " " + date.format('YYYY') + " เวลา : " + date.format('HH:mm:ss:SSS') + " ]";
  $("#show_time_channel_1").html("")
  $("#show_time_channel_2").html("")
  $("#show_time_channel_3").html("")


  $("#show_time_channel_1").append("CH 1 [ DEVICE ID: " + JSON.parse(device_id_get_php)[0] + " ] "+ data_text);
  $("#show_time_channel_2").append("CH 2 [ DEVICE ID: " + JSON.parse(device_id_get_php)[0] + " ] "+ data_text);
  $("#show_time_channel_3").append("CH 3 [ DEVICE ID: " + JSON.parse(device_id_get_php)[0] + " ] "+ data_text);


  // $("#show_time_channel_1").append("CH 1 [ DEVICE ID: <spen>" + JSON.parse(device_id_get_php.replace(new RegExp('\'', 'g'), '"',"\""))[0] +"</spen> ] "+ data_text);
  // $("#show_time_channel_2").append("CH 2 [ DEVICE ID: " + JSON.parse(device_id_get_php.replace(new RegExp('\'', 'g'), '"',"\""))[0] +" ] "+ data_text);
  // $("#show_time_channel_3").append("CH 3 [ DEVICE ID: " + JSON.parse(device_id_get_php.replace(new RegExp('\'', 'g'), '"',"\""))[0] +" ] "+ data_text);
  console.log('..get_realtime..');
  console.log("===============================")
  console.log(response)
  console.log("===============================")
  // var time_e = moment().format('x');
  // // var time_e = "1549343513317" //test
  // var time_s = parseInt(time_e)-2000;
  // time_s = time_s.toString();

  // // console.log(time_s);
  // // console.log(time_e);

  // var settings = {
  //   "async": true,
  //   "crossDomain": true,
  //   "url": "http://128.199.218.27:3000/api/data?id="+JSON.parse(device_id)[0]+"&st="+time_s+"&ed="+time_e,
  //   "method": "GET"
  // }
  // $.ajax(settings).done(function (response) {

    try {
      console.log(response);

      var graph_ch1_x=[],graph_ch1_y=[];
      var graph_ch2_x=[],graph_ch2_y=[];
      var graph_ch3_x=[],graph_ch3_y=[];

      var utc_plus=0;
      var utc_plus_val=0;

      for(var index=0;index<1;index++){
        // console.log("index : "+index);
        var val = response.payload;

        utc_plus = response.UTC;
        utc_plus_val = Math.floor((1/response.sampling)*(1000));
        // console.log(utc_plus_val);

        for(var index2=0;index2<val.length;index2++){
          // console.log(val[index2].data);
          // console.log(val[index2].data.length);
          for(var index3=0;index3<val[index2].data.length;index3++){
            // console.log(val[index2].data[index3]);
            // console.log(val[index2].ch);
            // console.log(moment(utc_plus).format("H:mm:ss:SSS"))
            switch(parseInt(val[index2].ch)) {
              case 1:
              graph_ch1_x.push(moment(utc_plus).add(param_get_timezone, "hours").format("H:mm:ss:SSS"));
              graph_ch1_y.push(val[index2].data[index3]);
              break;
              case 2:
              graph_ch2_x.push(moment(utc_plus).add(param_get_timezone, "hours").format("H:mm:ss:SSS"));
              graph_ch2_y.push(val[index2].data[index3]);
              break;
              case 3:
              graph_ch3_x.push(moment(utc_plus).add(param_get_timezone, "hours").format("H:mm:ss:SSS"));
              graph_ch3_y.push(val[index2].data[index3]);
              break;
              default:
              console.log("Empty Value!!");
            }
            utc_plus+=utc_plus_val;
          }
        }
        utc_plus=0;
      }
      graph1(graph_ch1_x,graph_ch1_y);
      graph2(graph_ch2_x,graph_ch2_y);
      graph3(graph_ch3_x,graph_ch3_y);
    } catch (e) {
      console.log(e);
    } finally {
      graph1(graph_ch1_x,graph_ch1_y);
      graph2(graph_ch2_x,graph_ch2_y);
      graph3(graph_ch3_x,graph_ch3_y);
    }

  // });
}

function batt(val) {

  var font_size = "110px";
  $('#bat_img').empty();

  if(val>=100){
    $('#bat_img').append('<i class="fas fa-battery-full" style="font-size: '+font_size+'; color: #70db70;"></i>');
    $('#battery').text(val+"%");
  }else if(val>=75 && val<100){
    $('#bat_img').append('<i class="fas fa-battery-three-quarters" style="font-size: '+font_size+';"></i>');
    $('#battery').text(val+"%");
  }else if(val>=50 && val<=74){
    $('#bat_img').append('<i class="fas fa-battery-half" style="font-size: '+font_size+';"></i>');
    $('#battery').text(val+"%");
  }else if(val>=25 && val<=49){
    $('#bat_img').append('<i class="fas fa-battery-quarter" style="font-size: '+font_size+';"></i>');
    $('#battery').text(val+"%");
  }else if(val>=0 && val<=24){
    $('#bat_img').append('<i class="fas fa-battery-empty" style="font-size: '+font_size+'; color: #ff6666;"></i>');
    $('#battery').text(val+"%");
  }else{}
}
